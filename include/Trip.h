#ifndef LOOTTRACKER_TRIP_H
#define LOOTTRACKER_TRIP_H

#include <QDate>
#include <QMap>

#include "Duration.h"
#include "Supplies.h"

class QSettings;

class Trip
{
public:
    Trip();
    Trip(QSettings& aTripSettings);

    void save(QSettings& aSettings) const;

    const QDate& getTripDate() const;

    void setTripNumber(int tripNumber);
    int getTripNumber() const;

    const QString& getTripNotes() const;
    void setTripNotes(const QString& notes);

    void setDuration(const Duration& duration);
    const Duration& getDuration() const;

    void setSupplies(const Supplies& aSupplies);
    const Supplies& getSupplies() const;

    void setNumberKills(int aNumKills);
    int getNumberKills() const;

    void setLootValue(int aLootValue);
    int getLootValue() const;

    int getTotalProfit() const;
    int getProfitPerHour() const;
    int getProfitPerKill() const;
    int getKillsPerHour() const;

private:
    QDate mDate;
    int mTripNumber;
    QString mNotes;

    int mNumberKills;
    int mLootValue;

    Supplies mSupplies;
    Duration mDuration;

};

typedef QMap<int, Trip> TripMap;
#endif
