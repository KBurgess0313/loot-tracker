#ifndef WORLDCONFIGDIALOG_H_
#define WORLDCONFIGDIALOG_H_

#include <QDialog>

#include "World.h"

namespace Ui
{
  class WorldConfigDialog;
}

class WorldConfigDialog : public QDialog
{
 Q_OBJECT

public:
  WorldConfigDialog(QWidget* aParent = nullptr);
  ~WorldConfigDialog();

private slots:
  void addWorldClicked();
  void removeWorldClicked();
  void worldPingUpdate(const World& w);

private:
  void init();

  void addWorld(const World& world);

private:
  Ui::WorldConfigDialog* mpForm;
};

#endif //WORLDCONFIGDIALOG_H_
