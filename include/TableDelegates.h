#ifndef LOOTTRACKER_TABLEDELEGATES_H
#define LOOTTRACKER_TABLEDELEGATES_H

#include <QItemDelegate>

class QValidator;
class CurrencyValidator;
class QCompleter;

struct Statics
{
   public:
    Statics();
    ~Statics();

    static QValidator* mpIntValidator;
    static QCompleter* mpItemCompleter;
};

class BaseDelegate : public QItemDelegate
{
public:
    BaseDelegate(QWidget* parent = nullptr);
    virtual ~BaseDelegate() override;

    virtual void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override;

protected slots:
    void commitAndCloseEditor();
};

class UneditableDelegate : public BaseDelegate
{
public:
    UneditableDelegate(QWidget* parent = nullptr);
    virtual ~UneditableDelegate() override;

    virtual void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override;
};

class CompleterDelegate : public BaseDelegate
{
public:
    CompleterDelegate(QCompleter* completer, QWidget* parent = nullptr);
    virtual ~CompleterDelegate() override;

    virtual QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;


private:
    QCompleter* mpCompleter;
};

class IntegerDelegate : public BaseDelegate
{
public:
    IntegerDelegate(QWidget* parent = nullptr);
    virtual ~IntegerDelegate() override;

    virtual QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;
};


class CurrencyDelegate : public BaseDelegate
{
public:
    CurrencyDelegate(QWidget* parent = nullptr);
    virtual ~CurrencyDelegate() override;

    virtual QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;
};

#endif
