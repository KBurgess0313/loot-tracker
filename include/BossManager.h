#ifndef LOOTTRACKER_BOSSMANAGER_H
#define LOOTTRACKER_BOSSMANAGER_H

#include "Boss.h"
#include <QString>

class QSettings;

class BossManager : public QObject
{
    Q_OBJECT

public:
    static BossManager* instance();
    void release();

    void load(QSettings& load);
    void save(QSettings& save) const;

    Boss* getBoss(const QString& name);

    void createBoss(Boss newBoss);
    void removeBoss(const QString& bossName);

    QStringList getAllBossNames();

    int getTotalProfit() const;
signals:
    void bossCreated(Boss newBoss);
    void bossDeleted(const QString& oldBoss);

private:
    BossManager();
    ~BossManager();

private:
    BossMap mAllBosses;
    static BossManager* mpsInstance;
};

#endif //LOOTTRACKER_BOSSMANAGER_H
