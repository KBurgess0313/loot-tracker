#ifndef LOOTTRACKER_DURATION_H
#define LOOTTRACKER_DURATION_H

#include <QString>

class QSettings;

class Duration
{
public:
    Duration(QSettings& aSettings);
    Duration(int aTotalseconds);
    Duration(int hours, int minutes, int seconds);
    void save(QSettings& aSettings) const;

    // Overload + operator to add two Duration objects.
    Duration operator+(const Duration rhs);
    Duration& operator+=(const Duration& rhs);

    Duration& operator=(const QString& rhs);

    //Increments duration by one second.
    Duration& operator++();

    Duration operator/(int denom);
    Duration& operator/=(int denom);

    double getTotalHours() const;

    QString toString() const;

private:
    void normalize(int aMs);

private:
    int mTotalSeconds;
};

#endif
