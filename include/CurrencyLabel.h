#ifndef LOOTTRACKER_CURRENCYLABEL_H
#define LOOTTRACKER_CURRENCYLABEL_H

#include <QLabel>
#include "StringUtils.h"

class CurrencyLabel : public QLabel
{
    Q_OBJECT

public:
    explicit CurrencyLabel(const QString& text, QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    CurrencyLabel(QWidget* parent = nullptr,  Qt::WindowFlags f = Qt::WindowFlags());
    virtual ~CurrencyLabel() override;

    void setText(const currency::String& aText);

    void setValue(int aValue);
    int getValue() const;

private:
    int mValue;
};

#endif
