#ifndef WORLD_H_
#define WORLD_H_

#include <QObject>
#include <QFutureWatcher>

#include "LimitedQueue.h"

class QSettings;

class World : public QObject
{
  Q_OBJECT

public:
  World(int n);
  World(QSettings& settings);

  virtual ~World() override;

  bool operator <(const World& rhs);

  void save(QSettings& aSettings) const;

  int getWorldNumber() const;
  int getAveragePing() const;
  int getSampleSize() const;

signals:
  void pingComplete();

protected:
  virtual void timerEvent(QTimerEvent* e) override;

private slots:
  void pingFinished();

private:
  int mNumber;
  QString mUrl;
  LimitedQueue<int> mPingResults;
  int mAvgPing;
  double mStdDev;
  int mTimerId;

  QFutureWatcher<int> mFutureWatcher;
};

#endif //WORLD_H_
