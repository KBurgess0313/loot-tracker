#ifndef LOOTTRACKER_TRIPDIALOG_H
#define LOOTTRACKER_TRIPDIALOG_H

#include <QDialog>

#include "Boss.h"

#include "SaveFile.h"

namespace Ui {
    class TripDialog;
}

class TripDialog : public QDialog
{
    Q_OBJECT

public:
    TripDialog(const QString& bossName, QWidget* parent = nullptr);
    TripDialog(const QString& bossName, const Trip& previousTrip, QWidget* parent = nullptr);

    virtual ~TripDialog() override;

public slots:
    virtual void accept() override;
    virtual void reject() override;

    void tripInformationChanged();

    bool promptSaveConfirmation();

protected:
    virtual void keyPressEvent(QKeyEvent *event) override;

private:
    void attemptRestoreFromPreviousTrip();

private:
    Ui::TripDialog* mpForm;
    Boss* mpCurrentBoss;
    bool mIsEditable;
};

#endif //LOOTTRACKER_TRIPDIALOG_H
