#ifndef LOOTTRACKER_DURATIONWIDGET_H
#define LOOTTRACKER_DURATIONWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QElapsedTimer>
#include <QMetaObject>
#include "Duration.h"

namespace Ui {
class DurationWidget;
}

class DurationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DurationWidget(QWidget *parent = nullptr);
    explicit DurationWidget(int aSeconds, QWidget *parent = nullptr);
    explicit DurationWidget(const QString& aText, QWidget *parent = nullptr);
    virtual ~DurationWidget() override;

    void setDuration(const Duration& duration);
    const Duration& getDuration() const;

signals:
    void durationEdited();

private slots:
    void onDurationEdited();
    void onStartStopClicked();
    void timeout();

private:
    void init();
    void displayTime();

private:
    Ui::DurationWidget* mpForm;
    Duration mDuration;

    QTimer mTimer;

    QMetaObject::Connection mEditConnection;
};

#endif // DurationWidget_H
