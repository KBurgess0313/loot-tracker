#ifndef LOOTTRACKER_CURRENCYEDIT_H
#define LOOTTRACKER_CURRENCYEDIT_H

#include <QLineEdit>
#include "StringUtils.h"

class QValidator;

class CurrencyEdit : public QLineEdit
{
    Q_OBJECT

public:

    CurrencyEdit(QWidget *parent = nullptr);
    CurrencyEdit(const QString& contents, QWidget *parent = nullptr);

    virtual ~CurrencyEdit() override;

    void setValue(int aValue);
    int getValue() const;

signals:
    void currencyEditingFinished();

private slots:
    void onTextEdited();
    void onEditingFinished();

private:
    void initValidator();

private:
    int mValue;
    QValidator* mpValidator;
};

#endif
