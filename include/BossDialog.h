#ifndef LOOTTRACKER_BOSSDIALOG_H
#define LOOTTRACKER_BOSSDIALOG_H

#include <QDialog>

#include "Boss.h"

#include "SaveFile.h"

namespace Ui {
class BossDialog;
}

class BossDialog : public QDialog
{
    Q_OBJECT

public:
    BossDialog(QWidget* parent = nullptr);
    virtual ~BossDialog() override;

protected:
    virtual void keyPressEvent(QKeyEvent *event) override;

public slots:
    virtual void accept() override;

private:
    Ui::BossDialog* mpForm;
};

#endif //LOOTTRACKER_BOSSDIALOG_H
