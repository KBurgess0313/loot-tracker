#ifndef LOOTTRACKER_SupplyMANAGER_H
#define LOOTTRACKER_SupplyMANAGER_H

#include <QStringList>
#include "Supplies.h"

class QSettings;
class SupplyManager
{
public:
    static SupplyManager* instance();
    void release();

    void load(QSettings& load);
    void save(QSettings& save) const;

    void addSupplies(const QStringList& supplies);
    void removeSupplies(const QStringList& supplies);

    QStringList getAllKnownSupplies() const;

private:
    SupplyManager();
    ~SupplyManager();

private:
    QSet<QString> mSupplies;
    static SupplyManager* mpsInstance;
};

#endif //LOOTTRACKER_SupplyMANAGER_H
