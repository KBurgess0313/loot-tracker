#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "Boss.h"

#include "SaveFile.h"

namespace Ui {
class MainWindow;
}

class AboutDialog;
class BossDialog;
class SupplyManagerDialog;
class TripDialog;
class TripHistoryDialog;
class WorldConfigDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

private slots:
    void createBossClicked();
    void deleteBossClicked();
    void bossCreated( Boss newBoss);
    void bossDeleted( const QString& bossName);
    void bossSelected(const QString& bossName);
    void newTripClicked();
    void showAboutDialog();
    void showTripHistory();
    void showSupplyManager();
    void showWorldConfiguration();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    void load();
    void save();

    void refreshUi();
    void calculateTotalProfit();

private:
    Ui::MainWindow* mpForm;

    AboutDialog* mpAboutDialog;
    BossDialog* mpBossDialog;
    SupplyManagerDialog* mpSupplyManagerDialog;
    TripDialog* mpTripDialog;
    TripHistoryDialog* mpTripHistoryDialog;
    WorldConfigDialog* mpWorldConfigurationDialog;

    QString mCurrentBossName;

};

#endif // MAINWINDOW_H
