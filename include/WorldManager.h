#ifndef WORLDMANAGER_H_
#define WORLDMANAGER_H_

#include <QList>
#include <QObject>

class QSettings;
class World;

class WorldManager : public QObject
{
  Q_OBJECT

public:
  static WorldManager* instance();
  static void release();

  void load(QSettings& load);
  void save(QSettings& save) const;

  void addWorld(World* world);
  void removeWorld(int worldNumber);

  const QList<World*>& getSupportedWorlds() const;
  const World* getRecommendedWorld() const;

signals:
  //Emitted when we've found the new best world
  //based on the lowest ping value
  void suggestedWorld(const World& world);

  //Emitted for every world after we ping it.
  void pingUpdate(const World& world);

private slots:
  void handlePingResults();

private:
  WorldManager();
  ~WorldManager();

private:
  static WorldManager* mpsInstance;

  QList<World*> supportedWorlds;
  World* currentSuggestedWorld;
};

#endif
