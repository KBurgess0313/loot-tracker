#ifndef LOOTTRACKER_BOSS_H
#define LOOTTRACKER_BOSS_H

#include <QMap>
#include <QString>

#include "Trip.h"

class QSettings;

struct AverageTrip
{
    AverageTrip() :
        profit(0),
        profitPerHour(0),
        profitPerKill(0),
        killsPerHour(0),
        length(0)
    {
    }

    int profit;
    int profitPerHour;
    int profitPerKill;
    int killsPerHour;
    Duration length;
};

class Boss
{
public:
    Boss();
    Boss(const QString& name);
    Boss(QSettings& aSettings);

    void save(QSettings& aSettings) const;

    AverageTrip getAverageTrip() const;

    void addTrip( Trip trip);
    void removeTrip(int tripNumber);

    const TripMap& getAllTrips() const;

    void setName(const QString& aName);
    const QString& getName() const;

    void setHasPet(bool aHasPet);
    bool hasPet() const;

    void setOriginalKillCount(int aKc);
    int getOriginalKillCount() const;

    void setOriginalProfit(int aProfit);
    int getOriginalProfit() const;

    //Enter the number of kills before expected
    //drop.  I.e, 1/3000 drop rate, enter 3000).
    void setPetDropRate(int dropRate);

    int getTotalProfit() const;
    int getTotalKills() const;
    int getTotalNumberTrips() const;
    Duration getTotalDuration() const;

    double getLifetimePetPercentage() const;

private:
    QString mName;
    int mOriginalKillCount;
    int mOriginalProfit;
    bool mHasPet;
    double mPetDropRate;

    TripMap mAllTrips;
};

Q_DECLARE_METATYPE(Boss);

typedef QMap<QString, Boss> BossMap;

#endif //LOOTTRACKER_BOSS_H
