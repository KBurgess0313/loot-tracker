#ifndef LOOTTRACKER_SUPPLYTABLEWIDGET_H
#define LOOTTRACKER_SUPPLYTABLEWIDGET_H

#include <QWidget>

#include "Supplies.h"

namespace Ui {
class SupplyTableWidget;
}

class QTableWidget;
class QTableWidgetItem;

struct SupplyRow;

class SupplyTableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SupplyTableWidget(QWidget *parent = nullptr);
    virtual ~SupplyTableWidget() override;

    Supplies getAllSupplies() const;
    void setSupplies(const Supplies& supplies, bool historical = false);

signals:
    void totalCostUpdated(int totalCost);

protected:
    virtual void keyPressEvent(QKeyEvent *event) override;

private slots:
    void addSupplyClicked();
    void removeSupplyClicked();

    void cellUpdated(int row, int column);

private:
    void updateTotalCost();

private:
    Ui::SupplyTableWidget* mpForm;
    QList<SupplyRow*> mSupplyWidgets;
};

#endif // SupplyTable_H
