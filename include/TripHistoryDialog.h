#ifndef LOOTTRACKER_TRIPHISTORYDIALOG_H
#define LOOTTRACKER_TRIPHISTORYDIALOG_H

#include <QDialog>
#include <QTableWidgetItem>

namespace Ui {
class TripHistoryDialog;
}

class TripDialog;
class Boss;
struct TripRow;

class TripHistoryDialog : public QDialog
{
    Q_OBJECT

public:
    TripHistoryDialog(const QString& bossName, QWidget* parent = nullptr);
    virtual ~TripHistoryDialog() override;

public slots:
    void expandTrip(const QModelIndex& index);
    void removeTripClicked();

private:
    Ui::TripHistoryDialog* mpForm;
    Boss* mpBoss;
    TripDialog* mpTripDialog;
    QList<TripRow*> mTripRows;
};

#endif //LOOTTRACKER_TRIPHISTORYDIALOG_H
