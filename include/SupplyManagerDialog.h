#ifndef LOOTTRACKER_SUPPLYMANAGERDIALOG_H
#define LOOTTRACKER_SUPPLYMANAGERDIALOG_H

#include <QDialog>

namespace Ui {
class SupplyManagerDialog;
}

class SupplyManagerDialog : public QDialog
{
    Q_OBJECT

public:
    SupplyManagerDialog(QWidget* parent = nullptr);
    virtual ~SupplyManagerDialog() override;

public slots:
    void addItemClicked();
    void removeItemClicked();

private:
    Ui::SupplyManagerDialog* mpForm;
};

#endif //LOOTTRACKER_SUPPLYMANAGERDIALOG_H
