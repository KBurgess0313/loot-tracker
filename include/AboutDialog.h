#ifndef LOOTTRACKER_ABOUT_H
#define LOOTTRACKER_ABOUT_H

#include <QDialog>

namespace Ui
{
    class AboutDialog;
}

class AboutDialog : public QDialog
{
public:
    AboutDialog(QWidget* parent = nullptr);
    virtual ~AboutDialog() override;

private:
    Ui::AboutDialog* mpForm;
};

#endif // LOOTTRACKER_ABOUT_H
