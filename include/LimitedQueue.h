#ifndef QUEUE_H_
#define QUEUE_H_

#include <algorithm>
#include <assert.h>
#include <QVector>

template<class T>
class LimitedQueue
{
public:
  LimitedQueue(int maxSize, T defaultValue) :
    mQueue(QVector<T>(maxSize, defaultValue)),
    mMaxSize(maxSize),
    mCurrentIndex(0),
    mSize(0)
  {
  }

  ~LimitedQueue()
  {
  }

  void add(const T& value)
  {
    mQueue[mCurrentIndex++] = value;

    if(mCurrentIndex == mMaxSize) {
      mCurrentIndex = 0;
    }
    else {
      mSize++; //Once the queue is full, no need to keep incrementing
               //our queue size.  it's at max capacity.
    }
  }

  T getAverage() const
  {
    assert(mSize > 0);
    return std::accumulate(mQueue.begin(), mQueue.end(), 0) / mSize;
  }

  double getStdDeviation() const
  {
    T avg = getAverage();

    T sum = 0;
    double stdDev = 0.0;

    for(int i = 0; i < mSize; ++i)
    {
      T dev = (mQueue[i] - avg);
      sum += (dev * dev);
    }

    if(avg > 0)
    {
      double variance = sum / static_cast<double>(mSize);
      stdDev = sqrt(variance);
    }

    return stdDev;
  }

  int size() const
  {
    return mSize;
  }

private:
  QVector<T> mQueue;
  int mMaxSize;
  int mCurrentIndex;
  int mSize;
};

#endif //WORLD_H_
