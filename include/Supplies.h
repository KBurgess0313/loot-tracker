#ifndef LOOTTRACKER_SUPPLIES_H
#define LOOTTRACKER_SUPPLIES_H

#include <QList>
#include <QString>

class QSettings;

struct Item
{
public:
    Item(QSettings& aItem);
    Item(const QString& name, int price, int brought, int remaining);

    void save(QSettings& aItem) const;

    const QString& getName() const;

    void setPrice(const int aPrice);
    int getPrice() const;

    void setAmountBrought(const int brought);
    const int getAmountBrought() const;

    void setAmountRemaining(const int remaining);
    const int getAmountRemaining() const;

    int getAmountUsed() const;

    int getCost() const;

private:
    QString mName;
    int mPrice;
    int mBrought;
    int mRemaining;
};

typedef QList<Item> SuppliesList;

class Supplies
{
public:
    Supplies(const SuppliesList& aSupplies);
    Supplies(QSettings& aTripSettings);
    Supplies(){}

    void save(QSettings& aTripSettings) const;

    void addItem(const Item& aItem);
    void removeItem(const QString& name);

    int getTripCost() const;

    const SuppliesList& getSuppliesList() const;

private:
    SuppliesList mSupplies;
};

#endif // LOOTTRACKER_SUPPLIES_H
