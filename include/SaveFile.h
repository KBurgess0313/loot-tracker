#ifndef LOOTTRACKER_SAVEFILE_H
#define LOOTTRACKER_SAVEFILE_H

#include <QSettings>

class SaveFile
{
public:
    SaveFile();
    ~SaveFile();

public:
    void save();
    void load();

private:
    QSettings mSettings;
};

#endif // LOOTTRACKER_SAVEFILE_H
