#ifndef LOOTTRACKER_STRINGUTILS_H
#define LOOTTRACKER_STRINGUTILS_H

#include <QString>
#include <QLocale>

//forward declarations
class QDate;

namespace date
{
    class String : public QString
    {
    public:
        String();
        String(const QString& aRhs);
        String(const QDate& aDated);

        QDate toDate() const;
    };
}

namespace currency
{
    class String : public QString
    {
    public:
        String();
        String(const QChar *unicode, int size = -1);
        String(QChar ch);
        String(const QString &aRhs);

        static QString toString(int aValue);
        static QString toString(double aValue);
        int toInt(bool* ok) const;

    private:
        static QLocale mLocale;
    };
}


#endif //LOOTTRACKER_STRINGUTILS_H
