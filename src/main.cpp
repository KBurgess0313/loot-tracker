#include "mainwindow.h"
#include <QApplication>

#include <QDebug>
#include <QDir>

int main(int argc, char *argv[])
{
    QCoreApplication::addLibraryPath("./");
    QApplication a(argc, argv);

    QFile file(QDir::currentPath() + "/styles/dark.css");

    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());

    qApp->setStyleSheet(styleSheet);

    MainWindow w;

    w.show();

    return a.exec();
}
