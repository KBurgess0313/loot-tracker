#include "BossManager.h"

#include <QSettings>
#include "SaveFile.h"

namespace
{
    const QString BOSS_HEADER = "bosses";
}

BossManager* BossManager::mpsInstance = nullptr;

BossManager* BossManager::instance()
{
    if(mpsInstance == nullptr)
    {
        mpsInstance = new BossManager;
    }

    return mpsInstance;
}

void BossManager::release()
{
    if(mpsInstance)
    {
        delete mpsInstance;
    }
}

BossManager::BossManager()
{
}

BossManager::~BossManager()
{
}

void BossManager::load(QSettings& load)
{
    int numBosses = load.beginReadArray(BOSS_HEADER);
    for(int i = 0; i < numBosses; ++i)
    {
        load.setArrayIndex(i);
        Boss b = Boss(load);

        mAllBosses.insert(b.getName(), b);
        emit bossCreated(b);
    }

    load.endArray();
}

void BossManager::save(QSettings& save) const
{
    save.beginWriteArray(BOSS_HEADER);

    int index = 0;
    foreach(const Boss& boss, mAllBosses)
    {
        save.setArrayIndex(index);

        boss.save(save);
        index++;
    }

    save.endArray();
}

Boss* BossManager::getBoss(const QString& name)
{
    Boss* out = nullptr;

    if(mAllBosses.contains(name))
    {
        out = &mAllBosses[name];
    }

    return out;
}

void BossManager::createBoss(Boss newBoss)
{
    mAllBosses.insert(newBoss.getName(), newBoss);
    emit bossCreated(newBoss);

    SaveFile s;
    s.save();
}
void BossManager::removeBoss(const QString& bossName)
{
    if(mAllBosses.contains(bossName))
    {
        mAllBosses.remove(bossName);
        emit bossDeleted(bossName);
    }

    SaveFile s;
    s.save();
}

QStringList BossManager::getAllBossNames()
{
    return mAllBosses.keys();
}

int BossManager::getTotalProfit() const
{
    int totalProfit = 0;

    foreach(const Boss& b, mAllBosses)
    {
        totalProfit += b.getTotalProfit();
    }

    return totalProfit;
}
