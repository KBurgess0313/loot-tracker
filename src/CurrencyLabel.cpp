#include "CurrencyLabel.h"

CurrencyLabel::CurrencyLabel(const QString& text, QWidget* parent, Qt::WindowFlags f) :
    QLabel(parent, f),
    mValue(0)
{
    setText(text);
}

CurrencyLabel::CurrencyLabel(QWidget* parent, Qt::WindowFlags f) :
    QLabel(parent, f),
    mValue(0)
{
    setValue(0);
}

CurrencyLabel::~CurrencyLabel()
{

}

void CurrencyLabel::setText(const currency::String& aText)
{
    if(aText == "N/A")
    {
        QLabel::setText(aText);
        mValue = -1;
        return;
    }
    else
    {
        bool ok = true;
        int value = aText.toInt(&ok);
        value = (ok ? value : 0);

        setValue(value);
    }
}

void CurrencyLabel::setValue(int aValue)
{
    mValue = aValue;
    QLabel::setText(currency::String::toString(mValue));
}

int CurrencyLabel::getValue() const
{
    return mValue;
}
