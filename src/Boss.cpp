#include "Boss.h"

#include <QSettings>

#include "SaveFile.h"

namespace
{
    const QString NAME = QString("name");
    const QString HAS_PET = QString("has_pet");
    const QString DROP_RATE = QString("drop_rate");
    const QString TRIPS_HEADER = QString("trips");
    const QString ORIGINAL_KILL_COUNT = QString("original_kill_count");
    const QString ORIGINAL_PROFIT = QString("original_profit");
}

Boss::Boss() :
    mName(),
    mOriginalKillCount(0),
    mOriginalProfit(0),
    mHasPet(false),
    mPetDropRate(0.0),
    mAllTrips()
{
}

Boss::Boss(const QString& name) :
    mName(name),
    mOriginalKillCount(0),
    mOriginalProfit(0),
    mHasPet(false),
    mPetDropRate(0.0),
    mAllTrips()
{
}

Boss::Boss(QSettings& aSettings) :
    mName(aSettings.value(NAME, "error").toString()),
    mOriginalKillCount(aSettings.value(ORIGINAL_KILL_COUNT, 0).toInt()),
    mOriginalProfit(aSettings.value(ORIGINAL_PROFIT, 0).toInt()),
    mHasPet(aSettings.value(HAS_PET, false).toBool()),
    mPetDropRate(0.0)
{
    if(mHasPet)
    {
        mPetDropRate = aSettings.value(DROP_RATE, -1.0).toDouble();
    }

    int numTrips = aSettings.beginReadArray(TRIPS_HEADER);
    for(int i = 0; i < numTrips; ++i)
    {
        aSettings.setArrayIndex(i);
        Trip trip = Trip(aSettings);

        mAllTrips.insert(trip.getTripNumber(), trip);
    }

    aSettings.endArray();
}

void Boss::addTrip(Trip trip)
{
    int tripNumber = mAllTrips.size() + 1;
    trip.setTripNumber(tripNumber);

    mAllTrips.insert(tripNumber, trip);

    SaveFile f;
    f.save();
}

void Boss::removeTrip(int tripNumber)
{
    mAllTrips.remove(tripNumber);

    SaveFile s;
    s.save();
}

const TripMap& Boss::getAllTrips() const
{
    return mAllTrips;
}

void Boss::setName(const QString& aName)
{
    mName = aName;
}

const QString& Boss::getName() const
{
    return mName;
}

void Boss::save(QSettings& aSettings) const
{
    aSettings.setValue(NAME, mName);
    aSettings.setValue(HAS_PET, mHasPet);
    aSettings.setValue(ORIGINAL_KILL_COUNT, mOriginalKillCount);
    aSettings.setValue(ORIGINAL_PROFIT, mOriginalProfit);

    if(mHasPet)
    {
        aSettings.setValue(DROP_RATE, mPetDropRate);
    }

    aSettings.beginWriteArray(TRIPS_HEADER);

    int i = 0;

    foreach(const Trip& trip, mAllTrips)
    {
        aSettings.setArrayIndex(i);
        trip.save(aSettings);
        ++i;
    }

    aSettings.endArray();
}

AverageTrip Boss::getAverageTrip() const
{
    AverageTrip avgTrip;

    int numberTrips = mAllTrips.size();

    if(numberTrips > 0)
    {
        int numberKills = 0;
        int totalProfit = 0;
        Duration totalLength = Duration(0);

        foreach(const Trip& trip, mAllTrips)
        {
            numberKills += trip.getNumberKills();
            totalProfit += trip.getTotalProfit();
            totalLength += trip.getDuration();
        }

        avgTrip.profit = totalProfit / numberTrips;
        avgTrip.killsPerHour = static_cast<int>(numberKills / totalLength.getTotalHours());
        avgTrip.profitPerHour = static_cast<int>(totalProfit / totalLength.getTotalHours());
        avgTrip.profitPerKill = totalProfit / numberKills;
        avgTrip.length = totalLength / numberTrips;
    }
    return avgTrip;
}

void Boss::setHasPet(bool aHasPet)
{
    mHasPet = aHasPet;

    if(!mHasPet)
    {
        mPetDropRate = 0.0;
    }
}
bool Boss::hasPet() const
{
    return mHasPet;
}


void Boss::setOriginalKillCount(int aKc)
{
    mOriginalKillCount = aKc;
}

int Boss::getOriginalKillCount() const
{
    return mOriginalKillCount;
}

void Boss::setOriginalProfit(int aProfit)
{
    mOriginalProfit = aProfit;
}

int Boss::getOriginalProfit() const
{
    return mOriginalProfit;
}

void Boss::setPetDropRate(int dropRate)
{
    mPetDropRate = 1.0 / dropRate;
}

int Boss::getTotalProfit() const
{
    int totalProfit = mOriginalProfit;
    foreach(const Trip& t, mAllTrips)
    {
        totalProfit += t.getTotalProfit();
    }

    return totalProfit;
}

int Boss::getTotalKills() const
{
    int totalKills = mOriginalKillCount;
    foreach(const Trip& t, mAllTrips)
    {
        totalKills += t.getNumberKills();
    }

    return totalKills;
}
int Boss::getTotalNumberTrips() const
{
    return mAllTrips.size();
}

double Boss::getLifetimePetPercentage() const
{
    int totalKills = getTotalKills();

    double percentage = ((1 - pow(1 - mPetDropRate, totalKills)) * 100.0);

    return percentage;
}

Duration Boss::getTotalDuration() const
{
    Duration out(0);
    foreach(const Trip& trip, mAllTrips)
    {
        out += trip.getDuration();
    }

    return out;
}
