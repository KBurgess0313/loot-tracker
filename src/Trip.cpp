#include "Trip.h"

#include <QDateTime>
#include <QSettings>

#include "StringUtils.h"

namespace
{
    const QString TRIP_DATE = QString("trip_date");
    const QString TRIP_NUMBER = QString("trip_number");
    const QString NUMBER_KILLS = QString("number_kills");
    const QString LOOT_VALUE = QString("loot_value");
    const QString TRIP_NOTES = QString("notes");
}

Trip::Trip() :
    mDate(QDate::currentDate()),
    mTripNumber(-1),
    mNumberKills(0),
    mLootValue(0),
    mSupplies(),
    mDuration(0)
{
}

Trip::Trip(QSettings& aTripSettings) :
    mDate(date::String(aTripSettings.value(TRIP_DATE, QString()).toString()).toDate()),
    mTripNumber(aTripSettings.value(TRIP_NUMBER, -1).toInt()),
    mNotes(aTripSettings.value(TRIP_NOTES, "").toString()),
    mNumberKills(aTripSettings.value(NUMBER_KILLS, -1).toInt()),
    mLootValue(aTripSettings.value(LOOT_VALUE, -1).toInt()),
    mSupplies(aTripSettings),
    mDuration(aTripSettings)
{
}

void Trip::save(QSettings& aSettings) const
{
    aSettings.setValue(TRIP_DATE, date::String(mDate));
    aSettings.setValue(TRIP_NUMBER, mTripNumber);
    aSettings.setValue(TRIP_NOTES, mNotes);
    aSettings.setValue(NUMBER_KILLS, mNumberKills);
    aSettings.setValue(LOOT_VALUE, mLootValue);

    mSupplies.save(aSettings);
    mDuration.save(aSettings);
}

const QDate& Trip::getTripDate() const
{
    return mDate;
}

void Trip::setTripNumber(int tripNumber)
{
    mTripNumber = tripNumber;
}

int Trip::getTripNumber() const
{
    return mTripNumber;
}

const QString& Trip::getTripNotes() const
{
  return mNotes;
}

void Trip::setTripNotes(const QString& notes)
{
  mNotes = notes;
}

void Trip::setDuration(const Duration& duration)
{
    mDuration = duration;
}

const Duration& Trip::getDuration() const
{
    return mDuration;
}

void Trip::setSupplies(const Supplies& aSupplies)
{
    mSupplies = aSupplies;
}

const Supplies& Trip::getSupplies() const
{
    return mSupplies;
}

void Trip::setNumberKills(int aNumKills)
{
    mNumberKills = aNumKills;
}

int Trip::getNumberKills() const
{
    return mNumberKills;
}

void Trip::setLootValue(int aLootValue)
{
    mLootValue = aLootValue;
}

int Trip::getLootValue() const
{
    return mLootValue;
}

int Trip::getTotalProfit() const
{
    return mLootValue - mSupplies.getTripCost();
}

int Trip::getProfitPerHour() const
{
    return static_cast<int>(getTotalProfit() / mDuration.getTotalHours());
}

int Trip::getProfitPerKill() const
{
    return (getTotalProfit() / mNumberKills);
}

int Trip::getKillsPerHour() const
{
    return static_cast<int>(mNumberKills / mDuration.getTotalHours());
}
