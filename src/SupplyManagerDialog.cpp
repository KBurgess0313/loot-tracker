#include "SupplyManagerDialog.h"
#include "ui_SupplyManagerDialog.h"

#include <QInputDialog>

#include "SupplyManager.h"

SupplyManagerDialog::SupplyManagerDialog(QWidget* parent) :
    QDialog (parent),
    mpForm(new Ui::SupplyManagerDialog)
{
    mpForm->setupUi(this);

    const QStringList& knownSupplies = SupplyManager::instance()->getAllKnownSupplies();

    foreach(const QString item, knownSupplies)
    {
        mpForm->itemList->addItem(item);
    }

    mpForm->itemList->sortItems();

    connect(mpForm->btnAddItem, &QPushButton::clicked, this, &SupplyManagerDialog::addItemClicked);
    connect(mpForm->btnRemoveItem, &QPushButton::clicked, this, &SupplyManagerDialog::removeItemClicked);
}

SupplyManagerDialog::~SupplyManagerDialog()
{
    int numberItems = mpForm->itemList->count();

    QSet<QString> allItems;
    for(int i = 0; i < numberItems; ++i)
    {
        QListWidgetItem* item = mpForm->itemList->item(i);
        allItems.insert(item->text());

    }

    SupplyManager* supplyManager = SupplyManager::instance();

    QSet<QString> existingItems = supplyManager->getAllKnownSupplies().toSet();

    QSet<QString> addedItems = allItems - existingItems;
    QSet<QString> removedItems = existingItems - allItems;

    SupplyManager::instance()->addSupplies(addedItems.toList());
    SupplyManager::instance()->removeSupplies(removedItems.toList());
}

void SupplyManagerDialog::addItemClicked()
{
    QString text = QInputDialog::getText(this, "Create...", "Item Name:", QLineEdit::Normal);

    if(!text.isEmpty())
    {
        mpForm->itemList->addItem(text);
        mpForm->itemList->sortItems();
    }
}
void SupplyManagerDialog::removeItemClicked()
{
    QList<QListWidgetItem*> selectedItems = mpForm->itemList->selectedItems();

    foreach(QListWidgetItem* i, selectedItems)
    {
        int row = mpForm->itemList->row(i);
        delete mpForm->itemList->takeItem(row);
    }
}
