#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QMessageBox>
#include <QSettings>
#include <QString>

#include "AboutDialog.h"
#include "BossDialog.h"
#include "BossManager.h"
#include "SupplyManager.h"
#include "SupplyManagerDialog.h"
#include "TripDialog.h"
#include "TripHistoryDialog.h"
#include "World.h"
#include "WorldConfigDialog.h"
#include "WorldManager.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    mpForm(new Ui::MainWindow),
    mpAboutDialog(nullptr),
    mpBossDialog(nullptr),
    mpSupplyManagerDialog(nullptr),
    mpTripDialog(nullptr),
    mpTripHistoryDialog(nullptr),
    mpWorldConfigurationDialog(nullptr)
{
    mpForm->setupUi(this);

    mpForm->cboCurrentBoss->setInsertPolicy(QComboBox::InsertAlphabetically);

    connect(mpForm->actionExit, &QAction::triggered, qApp, &QApplication::closeAllWindows, Qt::QueuedConnection);
    connect(mpForm->actSupplyManager, &QAction::triggered, this, &MainWindow::showSupplyManager);
    connect(mpForm->actionAbout, &QAction::triggered, this, &MainWindow::showAboutDialog);
    connect(mpForm->actConfigureWorlds, &QAction::triggered, this, &MainWindow::showWorldConfiguration);

    connect(mpForm->cboCurrentBoss, &QComboBox::currentTextChanged, this, &MainWindow::bossSelected);

    connect(mpForm->btnCreateBoss, &QPushButton::clicked, this, &MainWindow::createBossClicked);
    connect(mpForm->btnDeleteBoss, &QPushButton::clicked, this, &MainWindow::deleteBossClicked);

    connect(mpForm->btnNewTrip, &QPushButton::clicked, this, &MainWindow::newTripClicked);
    connect(mpForm->btnTripHistory, &QPushButton::clicked, this, &MainWindow::showTripHistory);

    BossManager* bossMgr = BossManager::instance();
    connect(bossMgr, &BossManager::bossCreated, this, &MainWindow::bossCreated);
    connect(bossMgr, &BossManager::bossDeleted, this, &MainWindow::bossDeleted);

    connect(WorldManager::instance(), &WorldManager::suggestedWorld, [&](const World& w) {
      mpForm->lblSuggestedWorld->setText(QString::number(w.getWorldNumber()));
    });

    load();
    refreshUi();
}

MainWindow::~MainWindow()
{
    delete mpForm;
}

void MainWindow::createBossClicked()
{
    if(mpBossDialog)
    {
        delete mpBossDialog;
    }

    mpBossDialog = new BossDialog();
    mpBossDialog->show();
    showMinimized();

    connect(mpBossDialog, &BossDialog::finished, [&]()
    {
        mpBossDialog->deleteLater();
        mpBossDialog = nullptr;

        setWindowState(Qt::WindowActive);
    });
}

void MainWindow::deleteBossClicked()
{
    const QString& bossName = mpForm->cboCurrentBoss->currentText();

    QMessageBox msgBox;
    QString message = QString("Do you really want to delete %1?\n\nIf you continue, all saved data associated with this boss will be deleted.").arg(bossName);
    msgBox.setText(message);
    msgBox.setInformativeText("Do you want to continue?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);

    int ret = msgBox.exec();

    if(ret == QMessageBox::Yes)
    {
        BossManager::instance()->removeBoss(bossName);
    }
}

void MainWindow::bossCreated( Boss newBoss)
{
    const QString& bossName = newBoss.getName();

    mpForm->cboCurrentBoss->addItem(bossName);

    if(mpForm->cboCurrentBoss->count() > 0)
    {
        mpForm->btnNewTrip->setEnabled(true);
        mpForm->btnTripHistory->setEnabled(true);
    }

    calculateTotalProfit();
}

void MainWindow::bossDeleted(const QString& bossName)
{
    int index = mpForm->cboCurrentBoss->findText(bossName);

    BossManager::instance()->removeBoss(bossName);

    if(index >= 0)
    {
        mpForm->cboCurrentBoss->removeItem(index);

        if(mpForm->cboCurrentBoss->count() == 0)
        {
            mpForm->btnNewTrip->setEnabled(false);
            mpForm->btnTripHistory->setEnabled(false);
        }
    }
    calculateTotalProfit();
}

void MainWindow::bossSelected(const QString& bossName)
{
    mCurrentBossName = bossName;

    refreshUi();
}

void MainWindow::refreshUi()
{
    if(const Boss* boss = BossManager::instance()->getBoss(mCurrentBossName))
    {
        const AverageTrip& avgTrip = boss->getAverageTrip();

        mpForm->lblAverageProfit->setText(QString("$%1").arg(avgTrip.profit));
        mpForm->lblAverageLength->setText(avgTrip.length.toString());
        mpForm->lblAverageProfitPerHr->setValue(avgTrip.profitPerHour);
        mpForm->lblAverageProfitPerKill->setText(QString("$%1").arg(avgTrip.profitPerKill));
        mpForm->lblAvgKillsPerHr->setText(QString::number(avgTrip.killsPerHour));

        mpForm->lblTotalProfit->setValue(boss->getTotalProfit());
        mpForm->lblTotalKills->setText(QString::number(boss->getTotalKills()));
        mpForm->lblTotalTrips->setText(QString::number(boss->getTotalNumberTrips()));
        mpForm->lblTotalTime->setText(boss->getTotalDuration().toString());

        bool visible = boss->hasPet();

        mpForm->lblPet->setVisible(visible);
        mpForm->lblPetChance->setVisible(visible);

        if(visible)
        {
            mpForm->lblPetChance->setText(QString::number(boss->getLifetimePetPercentage(), 'g', 3) + "%");
        }
    }
    else
    {
        mpForm->lblAverageProfit->setText(QString("$%1").arg(0));
        mpForm->lblAverageLength->setText(QString("-:--:--"));
        mpForm->lblAverageProfitPerHr->setText(QString("$%1").arg(0));
        mpForm->lblAverageProfitPerKill->setText(QString("$%1").arg(0));
        mpForm->lblAvgKillsPerHr->setText(QString::number(0));

        mpForm->lblTotalProfit->setText(QString("$%1").arg(0));
        mpForm->lblTotalKills->setText(QString::number(0));
        mpForm->lblTotalTrips->setText(QString::number(0));
        mpForm->lblTotalTime->setText(QString("-:--:--"));

        mpForm->lblPet->setVisible(false);
        mpForm->lblPetChance->setVisible(false);
    }
}

void MainWindow::newTripClicked()
{
    const QString& selectedBoss = mpForm->cboCurrentBoss->currentText();

    if(mpTripDialog)
    {
      QMessageBox msgBox;
      msgBox.setWindowTitle("Cancel?");
      msgBox.setText("It looks like you already have a trip dialog open.  Do you wish to cancel the old trip?");
      msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
      msgBox.setDefaultButton(QMessageBox::Yes);

      int retVal = msgBox.exec();

      if(retVal == QMessageBox::No)
      {
        return;
      }

      delete mpTripDialog;
    }

    mpTripDialog = new TripDialog(selectedBoss);

    mpTripDialog->show();
    showMinimized();

    connect(mpTripDialog, &QDialog::finished, [&](int result)
    {
        if(result == QDialog::Accepted)
        {
            refreshUi();
            calculateTotalProfit();
        }

        mpBossDialog->deleteLater();
        mpTripDialog = nullptr;

        setWindowState(Qt::WindowActive);
    });
}

void MainWindow::showAboutDialog()
{
  if(mpAboutDialog)
  {
    delete mpAboutDialog;
  }

  mpAboutDialog = new AboutDialog();
  mpAboutDialog->show();

  connect(mpAboutDialog, &AboutDialog::finished, [&]() {
    mpAboutDialog->deleteLater();
    mpAboutDialog = nullptr;
  });
}

void MainWindow::showWorldConfiguration()
{
  if(mpWorldConfigurationDialog)
  {
    delete mpWorldConfigurationDialog;
  }

  mpWorldConfigurationDialog = new WorldConfigDialog();
  mpWorldConfigurationDialog->show();
  showMinimized();

  connect(mpWorldConfigurationDialog, &WorldConfigDialog::finished, [&]()
  {
    mpWorldConfigurationDialog->deleteLater();
    mpWorldConfigurationDialog = nullptr;

    setWindowState(Qt::WindowActive);
  });
}

void MainWindow::showTripHistory()
{
    if(mpTripHistoryDialog)
    {
        delete mpTripHistoryDialog;
    }

    mpTripHistoryDialog = new TripHistoryDialog(mCurrentBossName);
    mpTripHistoryDialog->show();

    showMinimized();

    connect(mpTripHistoryDialog, &TripHistoryDialog::finished, [&]()
    {
        mpTripHistoryDialog->deleteLater();
        mpTripHistoryDialog = nullptr;

        refreshUi();
        calculateTotalProfit();

        setWindowState(Qt::WindowActive);
    });
}

void MainWindow::showSupplyManager()
{
  if(mpSupplyManagerDialog)
  {
    delete mpSupplyManagerDialog;
  }

  mpSupplyManagerDialog = new SupplyManagerDialog();
  mpSupplyManagerDialog->show();
  showMinimized();

  connect(mpSupplyManagerDialog, &SupplyManagerDialog::finished, [&]()
  {
    mpSupplyManagerDialog->deleteLater();
    mpSupplyManagerDialog = nullptr;

    setWindowState(Qt::WindowActive);
  });
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  Q_UNUSED(event);

  if(mpWorldConfigurationDialog)
  {
    delete mpWorldConfigurationDialog;
    mpWorldConfigurationDialog = nullptr;
  }

  if(mpBossDialog)
  {
    delete mpBossDialog;
    mpBossDialog = nullptr;
  }

  if(mpTripDialog)
  {
    delete mpTripDialog;
    mpTripDialog = nullptr;
  }

  if(mpSupplyManagerDialog)
  {
    delete mpSupplyManagerDialog;
    mpSupplyManagerDialog = nullptr;
  }

  if(mpTripHistoryDialog)
  {
    delete mpTripHistoryDialog;
    mpTripHistoryDialog = nullptr;
  }
  
  WorldManager::instance()->release();
  BossManager::instance()->release();
  SupplyManager::instance()->release();
}


void MainWindow::load()
{
    SaveFile savedFile;

    savedFile.load();
}

void MainWindow::save()
{
    SaveFile savedFile;

    savedFile.save();
}

void MainWindow::calculateTotalProfit()
{
    int totalProfit = BossManager::instance()->getTotalProfit();
    mpForm->lblLifeTimeProfit->setText(QString("$%1").arg(totalProfit));
}
