#include "StringUtils.h"

#include <QDate>

namespace date
{
    const QString DATE_FORMAT = "yyyy-MM-dd";

    String::String() :
        QString ()
    {
    }

    String::String(const QString& aRhs) :
        QString(aRhs)
    {

    }
    String::String(const QDate& aDate)
    {
        QString* t = dynamic_cast<QString*>(this);
        *t = aDate.toString(DATE_FORMAT);
    }

    QDate String::toDate() const
    {
        QDate out = QDate::fromString(*this, DATE_FORMAT);
        return out;
    }
}

namespace currency
{
    QLocale String::mLocale = QLocale(QLocale::English, QLocale::UnitedStates);

    String::String() :
        QString()
    {
    }

    String::String(const QChar *unicode, int size) :
        QString(unicode, size)
    {
    }

    String::String(QChar ch) :
        QString(ch)
    {
    }

    String::String(const QString &aRhs) :
        QString(aRhs)
    {
    }

    QString String::toString(int aValue)
    {
        return mLocale.toCurrencyString(aValue);
    }

    QString String::toString(double aValue)
    {
        return mLocale.toCurrencyString(aValue);
    }

    int String::toInt(bool* ok) const
    {
        *ok = true;

        QString text = *(dynamic_cast<const QString*>(this));

        text.remove('$');
        text.remove(',');
        text.remove('.');

        int value = text.toInt(ok);

        if(!*ok)
        {
          const QString& textLower = text.toLower();
          if(textLower.contains('k'))
          {
            QStringList t = textLower.split('k');

            value = t[0].toInt();
            value *= 1000;
            *ok = true;
          }
          else if(textLower.contains('m'))
          {
            QStringList t = textLower.split('m');

            value = t[0].toInt();
            value *= 1000000;
            *ok = true;
          }
        }

        return value;
    }
}
