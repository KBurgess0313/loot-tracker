#include "SaveFile.h"

#include "BossManager.h"
#include "SupplyManager.h"
#include "WorldManager.h"

namespace
{
    const QString ORGANIZATION = "RS Utils";
    const QString APPLICATION = "Loot Tracker";
}

SaveFile::SaveFile() :
    mSettings(QSettings::IniFormat, QSettings::UserScope, ORGANIZATION, APPLICATION)
{
}

SaveFile::~SaveFile()
{
}

void SaveFile::save()
{
    mSettings.clear();

    BossManager::instance()->save(mSettings);
    SupplyManager::instance()->save(mSettings);
    WorldManager::instance()->save(mSettings);
}

void SaveFile::load()
{
    BossManager::instance()->load(mSettings);
    SupplyManager::instance()->load(mSettings);
    WorldManager::instance()->load(mSettings);
}
