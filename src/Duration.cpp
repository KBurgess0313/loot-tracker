#include "Duration.h"

#include <math.h>

#include <QSettings>
#include <QString>

namespace
{
    const QString DURATION = QString("duration");

    const double SECONDS_IN_HOUR = (3600.0);
    const int MAX_SECONDS = 360000; //99:59:59 will roll over to 0:0:0.
}

Duration::Duration(QSettings& aSettings)
{
    mTotalSeconds = aSettings.value(DURATION, 0).toInt();
}

Duration::Duration(int aTotalSeconds) :
    mTotalSeconds(aTotalSeconds)
{
}

Duration::Duration(int hours, int minutes, int seconds)
{
    mTotalSeconds = ((hours * 3600) + (minutes * 60) + (seconds));
}

void Duration::save(QSettings& aSettings) const
{
    aSettings.setValue(DURATION, mTotalSeconds);
}

double Duration::getTotalHours() const
{
    double totalHours = (mTotalSeconds / SECONDS_IN_HOUR);

    return totalHours;
}

QString Duration::toString() const
{
    int totalSeconds = mTotalSeconds;

    int hours = totalSeconds / 3600;
    totalSeconds = (totalSeconds - hours * 3600);

    int minutes  = totalSeconds / 60;
    totalSeconds = (totalSeconds - minutes * 60);

    int seconds = totalSeconds;

    QString out = QString("%1:%2:%3").arg(hours)
                                     .arg(minutes, 2, 10, QChar('0'))
                                     .arg(seconds, 2, 10, QChar('0'));

    return out;
}

Duration Duration::operator+(const Duration rhs)
{
    int sec = mTotalSeconds + rhs.mTotalSeconds;

    if(sec >= MAX_SECONDS)
    {
        sec = 0;
    }

    Duration out = Duration(sec);

    return out;
}

Duration& Duration::operator=(const QString& time)
{
    QStringList parts = time.split(":");

    int numberParts = parts.size();

    int hours = 0;
    int minutes = 0;
    int seconds = 0;

    if(numberParts >= 3)
    {
        hours = parts[0].toInt();
        minutes = parts[1].toInt();
        seconds = parts[2].toInt();
    }
    else if(numberParts >= 2)
    {
        minutes = parts[0].toInt();
        seconds = parts[1].toInt();
    }
    else if(numberParts == 1)
    {
        seconds = parts[0].toInt();
    }

    mTotalSeconds = (hours * 3600) + (minutes * 60) + (seconds);
    return *this;
}

Duration& Duration::operator+=(const Duration& rhs)
{
    mTotalSeconds += rhs.mTotalSeconds;
    if(mTotalSeconds >= MAX_SECONDS)
    {
        mTotalSeconds = 0;
    }

    return *this;
}

Duration& Duration::operator++()
{
    mTotalSeconds++;

    if(mTotalSeconds >= MAX_SECONDS)
    {
        mTotalSeconds = 0;
    }

    return *this;
}

Duration Duration::operator/(int denom)
{
    Duration out = Duration(0);

    if(denom == 0)
    {
        out.mTotalSeconds = -1;
    }
    else
    {
        out.mTotalSeconds = mTotalSeconds / denom;
    }

    return out;
}

Duration& Duration::operator/=(int denom)
{
    if(denom == 0)
    {
        mTotalSeconds = -1;
    }
    else
    {
        mTotalSeconds /= denom;
    }

    return *this;
}
