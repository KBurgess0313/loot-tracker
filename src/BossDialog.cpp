#include "BossDialog.h"

#include "ui_BossDialog.h"
#include "BossManager.h"

BossDialog::BossDialog(QWidget* parent) :
    QDialog (parent),
    mpForm(new Ui::BossDialog)
{
    mpForm->setupUi(this);
}

BossDialog::~BossDialog()
{
    delete mpForm;
    mpForm = nullptr;
}

void BossDialog::accept()
{
   const QString& bossName = mpForm->bossNameEdit->text();
   int currentKillCount = mpForm->currentKillCountSpin->value();
   bool hasPet = mpForm->grpPetInfo->isChecked();
   int petDropRate = mpForm->petDropRateSpin->value();
   int preExistingProfit = mpForm->preExistantProfit->getValue();

   Boss newBoss = Boss(bossName);
   newBoss.setOriginalKillCount(currentKillCount);
   newBoss.setHasPet(hasPet);
   newBoss.setOriginalProfit(preExistingProfit);

   if(hasPet)
   {
       newBoss.setPetDropRate(petDropRate);
   }

   BossManager::instance()->createBoss(newBoss);\

   QDialog::accept();
}

void BossDialog::keyPressEvent(QKeyEvent *event)
{
    if(event->key() != Qt::Key_Enter)
    {
        QDialog::keyPressEvent(event);
    }
}
