#include "CurrencyEdit.h"

#include <QRegExp>
#include <QRegExpValidator>

#include "StringUtils.h"

#include <QDebug>

namespace
{
    const QRegExp& CURRENCY_REG_EXP = QRegExp("^\\s*\\$?(\\d{1,3}(\\,\\d{3})*|(\\d+))?(k|K|m|M)?\\s*$");
}
CurrencyEdit::CurrencyEdit(QWidget *parent) :
    QLineEdit(parent),
    mValue(0),
    mpValidator(nullptr)
{
    connect(this, &CurrencyEdit::editingFinished, this, &CurrencyEdit::onEditingFinished);
    connect(this, &CurrencyEdit::textEdited, this, &CurrencyEdit::onTextEdited);
    initValidator();

    setValue(0);
}

CurrencyEdit::CurrencyEdit(const QString& contents, QWidget *parent) :
    QLineEdit(parent),
    mValue(0),
    mpValidator(nullptr)
{
    connect(this, &CurrencyEdit::editingFinished, this, &CurrencyEdit::onEditingFinished);
    connect(this, &CurrencyEdit::textEdited, this, &CurrencyEdit::onTextEdited);
    initValidator();

    QString c = contents;
    int l = c.size();

    if (mpValidator->validate(c, l) == QValidator::Acceptable)
    {
        currency::String str = currency::String(contents);

        bool ok = true;
        int value = str.toInt(&ok);
        value = (ok ? value : 0);
        setValue(value);
    }
    else
    {
        setValue(0);
    }
}

CurrencyEdit::~CurrencyEdit()
{
}

void CurrencyEdit::setValue(int aValue)
{
    mValue = aValue;
    QLineEdit::setText(currency::String::toString(mValue));
}

int CurrencyEdit::getValue() const
{
	return mValue;
}

void CurrencyEdit::onTextEdited()
{
    currency::String newText = currency::String(text());

    bool ok = true;
    mValue = newText.toInt(&ok);

    mValue = (ok ? mValue : -1);
}

void CurrencyEdit::onEditingFinished()
{
    bool wasblocked = blockSignals(true);

    currency::String newText = currency::String(text());

    bool ok = true;
    mValue = newText.toInt(&ok);

    mValue = (ok ? mValue : -1);

    newText = currency::String::toString(mValue);
    setText(newText);

    blockSignals(wasblocked);

    emit currencyEditingFinished();
}

void CurrencyEdit::initValidator()
{
    mpValidator = new QRegExpValidator(CURRENCY_REG_EXP);
    setValidator(mpValidator);
}
