#include "TripDialog.h"

#include <QLocale>
#include <QMessageBox>
#include <QSpinBox>

#include "BossManager.h"
#include "StringUtils.h"
#include "SupplyManager.h"
#include "Trip.h"
#include "World.h"
#include "WorldManager.h"

#include "ui_TripDialog.h"

TripDialog::TripDialog(const QString& bossName, QWidget* parent) :
    QDialog (parent),
    mpForm(new Ui::TripDialog),
    mpCurrentBoss(BossManager::instance()->getBoss(bossName)),
    mIsEditable(true)
{
    mpForm->setupUi(this);

    setWindowFlags(windowFlags() | Qt::WindowMinimizeButtonHint);

    if(mpCurrentBoss)
    {
        setWindowTitle(QString("%1 Trip").arg(bossName));
    }

    WorldManager* wmgr = WorldManager::instance();

    if(const World* suggestedWorld = wmgr->getRecommendedWorld())
    {
      mpForm->lblSuggestedWorld->setText(QString::number(suggestedWorld->getWorldNumber()));
    }

    connect(wmgr, &WorldManager::suggestedWorld, [&](const World& w) {
      mpForm->lblSuggestedWorld->setText(QString::number(w.getWorldNumber()));
    });

    connect(mpForm->supplyTableWidget,
            static_cast<void (SupplyTableWidget::*)(int)>(&SupplyTableWidget::totalCostUpdated),
            this,
            static_cast<void (TripDialog::*)()>(&TripDialog::tripInformationChanged));

    connect(mpForm->totalLootEdit,
            static_cast<void (QLineEdit::*)(const QString&)>(&QLineEdit::textChanged),
            this,
            static_cast<void (TripDialog::*)()>(&TripDialog::tripInformationChanged));

    connect(mpForm->totalKillsSpin,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            this,
            static_cast<void (TripDialog::*)()>(&TripDialog::tripInformationChanged));

    connect(mpForm->durationWidget, &DurationWidget::durationEdited, this, &TripDialog::tripInformationChanged);

    attemptRestoreFromPreviousTrip();

    tripInformationChanged();
}

TripDialog::TripDialog(const QString& bossName, const Trip& previousTrip, QWidget* parent) :
    QDialog (parent),
    mpForm(new Ui::TripDialog),
    mpCurrentBoss(BossManager::instance()->getBoss(bossName)),
    mIsEditable(false)
{
    mpForm->setupUi(this);

    setWindowFlags(windowFlags() | Qt::WindowCloseButtonHint);

    mpForm->buttonBox->setStandardButtons(QDialogButtonBox::Close);

    //This is only used to show them a trip history.  They can't actually save anything.
    //Disable the widget, but leave the close button enabled.
    mpForm->grpSupplies->setEnabled(false);
    mpForm->grpTripInfo->setEnabled(false);

    //For historical trips, we don't need to see a suggested world.
    mpForm->lblSuggestedWorld->setVisible(false);
    mpForm->lblSuggestedWorldLbl->setVisible(false);

    if(mpCurrentBoss)
    {
        setWindowTitle(QString("%1 Trip %2").arg(bossName).arg(previousTrip.getTripNumber()));
    }

    mpForm->tripNotes->setText(previousTrip.getTripNotes());
    mpForm->supplyTableWidget->setSupplies(previousTrip.getSupplies(), true);
    mpForm->totalLootEdit->setValue(previousTrip.getLootValue());
    mpForm->totalKillsSpin->setValue(previousTrip.getNumberKills());
    mpForm->durationWidget->setDuration(previousTrip.getDuration());

    tripInformationChanged();
}

TripDialog::~TripDialog()
{
    delete mpForm;
    mpForm = nullptr;
}

void TripDialog::accept()
{
    bool performSave = promptSaveConfirmation();

    if(performSave)
    {
        Trip newTrip = Trip();

        const Supplies& supplies = mpForm->supplyTableWidget->getAllSupplies();

        newTrip.setSupplies(supplies);
        newTrip.setTripNotes(mpForm->tripNotes->toPlainText());
        newTrip.setDuration(mpForm->durationWidget->getDuration());
        newTrip.setLootValue(mpForm->totalLootEdit->getValue());
        newTrip.setNumberKills(mpForm->totalKillsSpin->value());

        if(mpCurrentBoss)
        {
            mpCurrentBoss->addTrip(newTrip);
        }

        QDialog::accept();
    }
}

void TripDialog::reject()
{
  bool closeDialog = true;

  if(mIsEditable)
  {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Cancel?");
    msgBox.setText("Do you want to cancel this trip?  All data will be erased.");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);

    int retVal = msgBox.exec();

    closeDialog = (retVal == QMessageBox::Yes);
  }

  if(closeDialog)
  {
    QDialog::reject();
  }
}

bool TripDialog::promptSaveConfirmation()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Proceed?");
    msgBox.setText("Once you have saved this trip, you will not be able to edit the data again.  Do you wish to proceed?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);

    int retVal = msgBox.exec();
    return (retVal == QMessageBox::Yes);
}

void TripDialog::keyPressEvent(QKeyEvent *event)
{
    if(event->key() != Qt::Key_Enter)
    {
        QDialog::keyPressEvent(event);
    }
}

void TripDialog::tripInformationChanged()
{
    const Duration& duration = mpForm->durationWidget->getDuration();
    const Supplies& supplies = mpForm->supplyTableWidget->getAllSupplies();

    int supplyCost = supplies.getTripCost();
    int killCount = mpForm->totalKillsSpin->value();
    int lootTotal = mpForm->totalLootEdit->getValue();

    QString supplyStr = currency::String::toString(supplyCost);

    QString lootStr = QString("$%1").arg(lootTotal);
    QString profitStr = QString("N/A");
    QString profitPerKillStr = QString("N/A");
    QString profitPerHourStr = QString("N/A");
    QString killsPerHourStr = QString("N/A");

    int profit = lootTotal - supplyCost;
    profitStr = QString("%1").arg(profit);

    double hours = duration.getTotalHours();
    if(hours > 0)
    {
        int profitPerHour = static_cast<int>(profit / hours);
        profitPerHourStr = QString("%1").arg(profitPerHour);

        double killsPerHour = killCount / hours;
        killsPerHourStr = QString("%1").arg(killsPerHour, 3, 'f', 2);
    }


    QString profitPerKill = QString("N/A");

    if(killCount > 0)
    {
        int pk = profit / killCount;
        profitPerKill = QString("$%1").arg(pk);
    }

    mpForm->lblSupplyCost->setText(supplyStr);
    mpForm->lblProfit->setText(profitStr);
    mpForm->lblProfitPerHr->setText(profitPerHourStr);
    mpForm->lblKillsPerHour->setText(killsPerHourStr);
    mpForm->lblProfitPerKill->setText(profitPerKill);
}

void TripDialog::attemptRestoreFromPreviousTrip()
{
    const TripMap& allBossTrips = mpCurrentBoss->getAllTrips();


    if(allBossTrips.size() > 0)
    {
        const Trip& lastTrip = allBossTrips.last();
        const Supplies& lastSupplies = lastTrip.getSupplies();

        if(!lastSupplies.getSuppliesList().isEmpty())
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Restore Supplies?");
            msgBox.setText("Do you want to re-use the same supplies from your last trip?");
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::Yes);

            int retVal = msgBox.exec();
            if(retVal == QMessageBox::Yes)
            {
                mpForm->supplyTableWidget->setSupplies(lastSupplies);
            }
        }
    }
}
