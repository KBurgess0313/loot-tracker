#include "DurationWidget.h"
#include "ui_DurationWidget.h"

#include <QLabel>
#include <QString>
#include <QTime>
#include <QDebug>
#include <QRegExp>

namespace
{
    const int ONE_SECOND_SLEEP = 1000;

    const QRegExp TIME_EXP = QRegExp("\\s*(?:(\\d{1,2}):([0-5]?\\d):)?([0-5]?\\d)\\s*");
}

DurationWidget::DurationWidget(QWidget *parent) :
    QWidget(parent),
    mpForm(new Ui::DurationWidget),
    mDuration(0)
{
    init();
}

DurationWidget::DurationWidget(int aSeconds, QWidget *parent) :
    QWidget(parent),
    mpForm(new Ui::DurationWidget),
    mDuration(aSeconds)
{
    init();
}

DurationWidget::DurationWidget(const QString& aText, QWidget* parent) :
    QWidget (parent),
    mpForm(new Ui::DurationWidget),
    mDuration(0)
{
    mDuration = aText;
    init();
}


DurationWidget::~DurationWidget()
{
    delete mpForm;
    mpForm = nullptr;
}

void DurationWidget::init()
{
    mpForm->setupUi(this);

    displayTime();

    connect(mpForm->btnStartStop, &QPushButton::clicked, this, &DurationWidget::onStartStopClicked);
    mEditConnection = connect(mpForm->totalTimeEdit, &QLineEdit::editingFinished, this, &DurationWidget::onDurationEdited);
    connect(&mTimer, &QTimer::timeout, this, &DurationWidget::timeout);

    mpForm->totalTimeEdit->setValidator(new QRegExpValidator(TIME_EXP, this));
}

void DurationWidget::setDuration(const Duration& duration)
{
    mDuration = duration;
    displayTime();
}

const Duration& DurationWidget::getDuration() const
{
    return mDuration;
}

void DurationWidget::displayTime()
{
    mpForm->totalTimeEdit->setText(mDuration.toString());
}

void DurationWidget::onStartStopClicked()
{
  QString newText;

  //If we were running, stop
  if (mTimer.isActive() )
  {
    mTimer.stop();
    mEditConnection = connect(mpForm->totalTimeEdit, &QLineEdit::textChanged, this, &DurationWidget::onDurationEdited);
    newText = "Start";
  }
  else
  {
    disconnect(mEditConnection);

    mTimer.setTimerType(Qt::PreciseTimer);
    mTimer.start(ONE_SECOND_SLEEP);

    newText = "Stop";
  }

  mpForm->btnStartStop->setText(newText);
}

void DurationWidget::timeout()
{
    ++mDuration;

    displayTime();
}

void DurationWidget::onDurationEdited()
{
    if(mTimer.isActive())
    {
        return;
    }

    mDuration = mpForm->totalTimeEdit->text();

    displayTime();

    emit durationEdited();
}
