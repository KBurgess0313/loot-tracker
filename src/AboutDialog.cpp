#include "AboutDialog.h"

#include "ui_AboutDialog.h"
#include <QDebug>
namespace
{
  const QString DATE = __DATE__;
  const QString VERSION = "v1.42";
}

AboutDialog::AboutDialog(QWidget* parent) :
    QDialog(parent),
    mpForm(new Ui::AboutDialog)
{
  mpForm->setupUi(this);

  mpForm->lblBuiltOnDate->setText(DATE);
  mpForm->lblVersion->setText(VERSION);
}

AboutDialog::~AboutDialog()
{
  delete mpForm;
  mpForm = nullptr;
}
