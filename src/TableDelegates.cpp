#include "TableDelegates.h"

#include <QCompleter>
#include <QLabel>
#include <QLineEdit>
#include <QIntValidator>
#include <QPainter>
#include <QRegExpValidator>
#include <QString>

#include "SupplyManager.h"
#include "CurrencyEdit.h"
#include "StringUtils.h"

namespace
{
    const QString NON_EDITABLE_STYLESHEET = "background-color: rgb(220, 220, 220)";
}

QValidator* Statics::mpIntValidator = nullptr;
QCompleter* Statics::mpItemCompleter = nullptr;

Statics::Statics()
{
    const QStringList& knownSupplies = SupplyManager::instance()->getAllKnownSupplies();

    mpIntValidator = new QIntValidator(nullptr);
    mpItemCompleter = new QCompleter(knownSupplies);
}

Statics::~Statics()
{
    delete mpIntValidator;
    delete mpItemCompleter;

    mpIntValidator = nullptr;
    mpItemCompleter = nullptr;
}

namespace
{
    Statics gStatics;
}


BaseDelegate::BaseDelegate(QWidget* parent) :
    QItemDelegate (parent)
{
}

BaseDelegate::~BaseDelegate()
{

}
void BaseDelegate::paint(QPainter *painter,
                         const QStyleOptionViewItem &option,
                         const QModelIndex &index) const
{
    QString text = index.model()->data(index, Qt::DisplayRole).toString();

    QStyleOptionViewItem myOption = option;
    myOption.displayAlignment = Qt::AlignHCenter | Qt::AlignVCenter;

    drawDisplay(painter, myOption, myOption.rect, text);
    drawFocus(painter, myOption, myOption.rect);
}

void BaseDelegate::commitAndCloseEditor()
{
    QWidget *e = qobject_cast<QWidget*>(sender());

    emit commitData(e);
    emit closeEditor(e);
}


UneditableDelegate::UneditableDelegate(QWidget* parent) :
    BaseDelegate (parent)
{
}

UneditableDelegate::~UneditableDelegate()
{

}

void UneditableDelegate::paint(QPainter *painter,
                   const QStyleOptionViewItem &option,
                   const QModelIndex &index) const
{
    painter->fillRect(option.rect, QBrush(QColor(64,64,64)));
    BaseDelegate::paint(painter, option, index);
}

CompleterDelegate::CompleterDelegate(QCompleter* completer, QWidget* parent) :
    BaseDelegate(parent),
    mpCompleter(completer)
{
}

CompleterDelegate::~CompleterDelegate()
{

}

QWidget* CompleterDelegate::createEditor(QWidget *parent,
                                       const QStyleOptionViewItem& option,
                                       const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QLineEdit* edit = new QLineEdit(parent);

    edit->setCompleter(mpCompleter);

    connect(edit, &QLineEdit::editingFinished,
            this, &CompleterDelegate::commitAndCloseEditor);

    return edit;
}

void CompleterDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    const QString& text = index.model()->data(index, Qt::DisplayRole).toString();
    QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
    edit->setText(text);
}

void CompleterDelegate::setModelData(QWidget *editor,
                                   QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QLineEdit* edit = qobject_cast<QLineEdit *>(editor);
    const QString& text = edit->text();

    model->setData(index, text);
}

IntegerDelegate::IntegerDelegate(QWidget* parent) :
    BaseDelegate(parent)
{
}

IntegerDelegate::~IntegerDelegate()
{

}

QWidget* IntegerDelegate::createEditor(QWidget *parent,
                                       const QStyleOptionViewItem& option,
                                       const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QLineEdit* edit = new QLineEdit(parent);

    QString text = index.model()->data(index, Qt::DisplayRole).toString();

    edit->setValidator(gStatics.mpIntValidator);
    edit->setText(text);

    connect(edit, &QLineEdit::editingFinished,
            this, &IntegerDelegate::commitAndCloseEditor);

    return edit;
}

void IntegerDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::DisplayRole).toString();
    QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
    edit->setText(value);
}

void IntegerDelegate::setModelData(QWidget *editor,
                                   QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QLineEdit* edit = qobject_cast<QLineEdit *>(editor);
    int value = edit->text().toInt();

    model->setData(index, value);
}

CurrencyDelegate::CurrencyDelegate(QWidget* parent) :
    BaseDelegate (parent)
{
}

CurrencyDelegate::~CurrencyDelegate()
{

}

QWidget* CurrencyDelegate::createEditor(QWidget *parent,
                                       const QStyleOptionViewItem& option,
                                       const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    CurrencyEdit* edit = new CurrencyEdit(parent);

    QString text = index.model()->data(index, Qt::DisplayRole).toString();

    edit->setText(text);

    connect(edit, &CurrencyEdit::currencyEditingFinished,
            this, &CurrencyDelegate::commitAndCloseEditor);

    return edit;
}

void CurrencyDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    currency::String val = index.model()->data(index, Qt::DisplayRole).toString();
    CurrencyEdit* edit = qobject_cast<CurrencyEdit*>(editor);
    edit->setText(val);
}

void CurrencyDelegate::setModelData(QWidget *editor,
                                   QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    CurrencyEdit* edit = qobject_cast<CurrencyEdit *>(editor);
    currency::String str = currency::String::toString(edit->getValue());

    model->setData(index, str);
}
