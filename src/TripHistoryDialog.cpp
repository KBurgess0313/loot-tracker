#include "TripHistoryDialog.h"

#include <QInputDialog>
#include <QMessageBox>

#include "Boss.h"
#include "BossManager.h"
#include "StringUtils.h"
#include "Trip.h"
#include "TableDelegates.h"
#include "TripDialog.h"

#include "ui_TripHistoryDialog.h"

namespace Columns
{
    enum Values
    {
        DATE = 0,
        NUMBER,
        LOOT,
        SUPPLY_COST,
        PROFIT,
        KILL_COUNT,
        LENGTH,

        BEGIN = DATE,
        END = LENGTH
    };
}

struct TripRow
{
public:
    TripRow(const Trip& aTrip) :
        mTripNumber(aTrip.getTripNumber())
    {
        for(int i = Columns::BEGIN; i <= Columns::END; ++i)
        {
            QTableWidgetItem* item = new QTableWidgetItem();
            item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);

            mAllItems.append(item);
        }

        mAllItems[Columns::DATE]->setText(date::String(aTrip.getTripDate()));
        mAllItems[Columns::NUMBER]->setText(QString::number(mTripNumber));
        mAllItems[Columns::LOOT]->setText(currency::String::toString(aTrip.getLootValue()));
        mAllItems[Columns::SUPPLY_COST]->setText(currency::String::toString(aTrip.getSupplies().getTripCost()));
        mAllItems[Columns::PROFIT]->setText(currency::String::toString(aTrip.getTotalProfit()));
        mAllItems[Columns::KILL_COUNT]->setText(QString::number(aTrip.getNumberKills()));
        mAllItems[Columns::LENGTH]->setText(aTrip.getDuration().toString());
    }

    ~TripRow()
    {
        mAllItems.clear();
    }

    int getTripNumber() const
    {
        return mTripNumber;
    }

    void addToTable(QTableWidget* table, int row)
    {
        for(int col = Columns::BEGIN; col <= Columns::END; ++col)
        {
            table->setItem(row, col, mAllItems[col]);
        }
    }

private:
    QList<QTableWidgetItem*> mAllItems;
    int mTripNumber;
};

TripHistoryDialog::TripHistoryDialog(const QString& bossName, QWidget* parent) :
    QDialog (parent),
    mpForm(new Ui::TripHistoryDialog),
    mpBoss(BossManager::instance()->getBoss(bossName)),
    mpTripDialog(nullptr)
{
    mpForm->setupUi(this);

    setWindowTitle(bossName + " Trip History");

    mpForm->tripTable->setColumnWidth(Columns::DATE, 125);
    mpForm->tripTable->setColumnWidth(Columns::NUMBER, 100);
    mpForm->tripTable->setColumnWidth(Columns::LOOT, 100);
    mpForm->tripTable->setColumnWidth(Columns::SUPPLY_COST, 100);
    mpForm->tripTable->setColumnWidth(Columns::PROFIT, 100);
    mpForm->tripTable->setColumnWidth(Columns::KILL_COUNT, 75);
    mpForm->tripTable->setColumnWidth(Columns::LENGTH, 100);

    connect(mpForm->tripTable, &QTableWidget::doubleClicked, this, &TripHistoryDialog::expandTrip);

    if(mpBoss)
    {
        const TripMap& aTrips = mpBoss->getAllTrips();

        int tripIndex = aTrips.size();

        int i = 0;

        mpForm->tripTable->setRowCount(tripIndex);

        //Add our trips in descending order, so the last trip appears at the top.
        for(; tripIndex > 0; --tripIndex)
        {
            TripRow* newRow = new TripRow(aTrips.value(tripIndex));

            newRow->addToTable(mpForm->tripTable, i++);

            mTripRows.append(newRow);
        }
    }
    connect(mpForm->btnRemoveTrip, &QPushButton::clicked, this, &TripHistoryDialog::removeTripClicked);
}

TripHistoryDialog::~TripHistoryDialog()
{
    qDeleteAll(mTripRows);
    mTripRows.clear();

    delete mpForm;
    mpForm = nullptr;

    delete mpTripDialog;
    mpTripDialog = nullptr;
}

void TripHistoryDialog::expandTrip(const QModelIndex& index)
{
    int row = index.row();
    int tripNumber = mTripRows.at(row)->getTripNumber();

    const Trip& trip = mpBoss->getAllTrips().value(tripNumber);

    if(mpTripDialog)
    {
        delete mpTripDialog;
    }

    mpTripDialog = new TripDialog(mpBoss->getName(), trip);

    connect(mpTripDialog, &QDialog::finished, [&]()
    {
        mpTripDialog->deleteLater();
        mpTripDialog = nullptr;
    });

    mpTripDialog->show();
}


void TripHistoryDialog::removeTripClicked()
{
    QModelIndexList selectedRows = mpForm->tripTable->selectionModel()->selectedRows();

    foreach(QModelIndex index, selectedRows)
    {
        int row = index.row();

        int tripNumber = mTripRows[row]->getTripNumber();
        QString message = QString("Do you really want to delete %1 Trip %2?\n\nIf you continue, this data can not be recovered again.").arg(mpBoss->getName())
                                                                                                                                       .arg(tripNumber);

        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);

        int ret = msgBox.exec();

        if(ret == QMessageBox::Yes)
        {
            mpForm->tripTable->removeRow(row);
            delete mTripRows.takeAt(row);

            mpBoss->removeTrip(tripNumber);
        }
    }
}
