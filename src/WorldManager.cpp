#include "WorldManager.h"

#include <QSettings>

#include "SaveFile.h"
#include "World.h"

#include <iostream>

namespace
{
  const QString WORLD_SETTINGS_HEADER = "supported_worlds";
}

WorldManager* WorldManager::mpsInstance = nullptr;

WorldManager* WorldManager::instance()
{
  if(mpsInstance == nullptr)
  {
    mpsInstance = new WorldManager();
  }

  return mpsInstance;
}

void WorldManager::release()
{
  delete mpsInstance;
  mpsInstance = nullptr;
}

void WorldManager::load(QSettings& load)
{
  int numWorlds = load.beginReadArray(WORLD_SETTINGS_HEADER);
  for(int i = 0; i < numWorlds; ++i)
  {
    load.setArrayIndex(i);
    World* w = new World(load);

    connect(w, &World::pingComplete, this, &WorldManager::handlePingResults);

    supportedWorlds.append(w);
  }

  load.endArray();
}

void WorldManager::save(QSettings& save) const
{
  save.beginWriteArray(WORLD_SETTINGS_HEADER);

  int index = 0;
  foreach(const World* w, supportedWorlds)
  {
    save.setArrayIndex(index);
    w->save(save);
    index++;
  }

  save.endArray();
}

void WorldManager::addWorld(World* world)
{
  connect(world, &World::pingComplete, this, &WorldManager::handlePingResults);
  supportedWorlds.append(world);

  SaveFile s;
  s.save();
}

void WorldManager::removeWorld(int worldNumber)
{
  int i = 0;
  bool found = false;
  for(;i < supportedWorlds.size(); ++i)
  {
    if(supportedWorlds[i]->getWorldNumber() == worldNumber)
    {
      found = true;
      break;
    }
  }

  if(found)
  {
    delete supportedWorlds.takeAt(i);
  }

  SaveFile s;
  s.save();
}

void WorldManager::handlePingResults()
{
  World* world = dynamic_cast<World*>(sender());

  emit pingUpdate(*world);

  //IF we don't have a suggested world, OR the new world has a better average
  //ping, thats our new world.
  if( (currentSuggestedWorld == nullptr) ||
      (*world < *currentSuggestedWorld))
  {
    currentSuggestedWorld = world;
    emit suggestedWorld(*currentSuggestedWorld);
  }
}

const QList<World*>& WorldManager::getSupportedWorlds() const
{
  return supportedWorlds;
}

const World* WorldManager::getRecommendedWorld() const
{
  return currentSuggestedWorld;
}

WorldManager::WorldManager() :
  currentSuggestedWorld(nullptr)
{
}

WorldManager::~WorldManager()
{
  qDeleteAll(supportedWorlds);
  supportedWorlds.clear();
}
