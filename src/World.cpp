#include "World.h"

#include <algorithm>
#include <QSettings>
#include <QtConcurrent>

#include "math.h"

#include "PingUtility.h"

namespace
{
  static const int MAX_QUEUE_SIZE = 120;
  const QString WORLD_HEADER = "world";
  const int PING_RATE_MS = 1000;
}

World::World(QSettings& settings) :
  World(settings.value(WORLD_HEADER, -1).toInt())
{
}

World::World(int n) :
  mNumber(n),
  mUrl(QString("oldschool%1.runescape.com").arg(mNumber - 300)),
  mPingResults(MAX_QUEUE_SIZE, 0),
  mAvgPing(-1),
  mStdDev(-1.0),
  mTimerId(-1)
{
  connect(&mFutureWatcher, &QFutureWatcher<int>::finished, this, &World::pingFinished);

  mTimerId = startTimer(PING_RATE_MS);
}

World::~World()
{
  if(mTimerId != -1)
  {
    killTimer(mTimerId);
  }

  disconnect(&mFutureWatcher, &QFutureWatcher<int>::finished, this, &World::pingFinished);

  mFutureWatcher.cancel();
}


bool World::operator <(const World& rhs)
{
  bool lessThan = false;

  if(mAvgPing < rhs.mAvgPing) {
    lessThan = true;
  }
  else if(mAvgPing == rhs.mAvgPing) {
    lessThan = (mStdDev < rhs.mStdDev);
  }

  return lessThan;
}

void World::timerEvent(QTimerEvent* e)
{
  Q_UNUSED(e);

  if(mFutureWatcher.isFinished())
  {
    mFutureWatcher.setFuture(QtConcurrent::run(pingUrl, mUrl));
  }
}

void World::save(QSettings& aSettings) const
{
  aSettings.setValue(WORLD_HEADER, mNumber);
}

int World::getWorldNumber() const
{
  return mNumber;
}

int World::getSampleSize() const
{
  return mPingResults.size();
}

int World::getAveragePing() const
{
  return mAvgPing;
}

void World::pingFinished()
{
  int ping = mFutureWatcher.result();

  if(ping != -1)
  {
    mPingResults.add(ping);
    mAvgPing = mPingResults.getAverage();
    mStdDev = mPingResults.getStdDeviation();

    //Emit that this world has a new ping result.
    emit pingComplete();
  }
}

