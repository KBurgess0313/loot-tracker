#include "SupplyManager.h"

#include <QSettings>

#include "SaveFile.h"

namespace
{
    const QString SUPPLY_HEADER = "supplies";
    const QString ITEM_HEADER = "item";
}

SupplyManager* SupplyManager::mpsInstance = nullptr;

SupplyManager* SupplyManager::instance()
{
    if(mpsInstance == nullptr)
    {
        mpsInstance = new SupplyManager;
    }

    return mpsInstance;
}

void SupplyManager::release()
{
    if(mpsInstance)
    {
        delete mpsInstance;
    }
}


SupplyManager::SupplyManager()
{
}

SupplyManager::~SupplyManager()
{
}

void SupplyManager::addSupplies(const QStringList& supplies)
{
    foreach(const QString& item, supplies)
    {
        if (!mSupplies.contains(item))
        {
            mSupplies.insert(item);
        }
    }

    SaveFile s;
    s.save();
}

void SupplyManager::removeSupplies(const QStringList& supplies)
{
    foreach(const QString& item, supplies)
    {
        mSupplies.remove(item);
    }

    SaveFile s;
    s.save();
}

QStringList SupplyManager::getAllKnownSupplies() const
{
    return QStringList(mSupplies.toList());
}

void SupplyManager::load(QSettings& load)
{
    int numSupplies = load.beginReadArray(SUPPLY_HEADER);
    for(int i = 0; i < numSupplies; ++i)
    {
        load.setArrayIndex(i);
        mSupplies.insert(load.value(ITEM_HEADER, "error").toString());
    }

    load.endArray();
}

void SupplyManager::save(QSettings& save) const
{
    save.beginWriteArray(SUPPLY_HEADER);

    int index = 0;
    foreach(const QString& item, mSupplies)
    {
        save.setArrayIndex(index);

        save.setValue(ITEM_HEADER, item);
        index++;
    }

    save.endArray();
}
