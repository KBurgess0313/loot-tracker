#include "SupplyTableWidget.h"

#include "ui_SupplyTableWidget.h"

#include <QCompleter>
#include <QLineEdit>
#include <QSpinBox>
#include <QLabel>
#include <QDebug>

#include "SupplyManager.h"
#include "TableDelegates.h"
#include "StringUtils.h"

namespace Columns
{
    enum Values
    {
        ITEM = 0,
        PRICE,
        BROUGHT,
        REMAINING,
        QUANTITY,
        COST,

        BEGIN = ITEM,
        END = COST
    };
}

struct SupplyRow
{
public:
    SupplyRow()
    {
        init();

        mAllItems[Columns::ITEM]->setText("New Item");
        mAllItems[Columns::PRICE]->setText("$0");
        mAllItems[Columns::BROUGHT]->setText("0");
        mAllItems[Columns::REMAINING]->setText("0");

        updateQuantity();
        updateCost();
    }

    SupplyRow(const Item& aItem, bool historicalTrip = false)
    {
        init();

        mAllItems[Columns::ITEM]->setText(aItem.getName());
        mAllItems[Columns::PRICE]->setText(currency::String::toString(aItem.getPrice()));

        if(!historicalTrip)
        {
          //When we're constructing a new trip based off the last one, we will automatically
          //populate the amount left over from the last trip as the new starting quantity.
          mAllItems[Columns::BROUGHT]->setText(QString::number(aItem.getAmountRemaining()));
          mAllItems[Columns::REMAINING]->setText("0");
        }
        else
        {
          //When we're constructing a previous trip based off the last one,use the recorded
          //start/end quantities
          mAllItems[Columns::BROUGHT]->setText(QString::number(aItem.getAmountBrought()));
          mAllItems[Columns::REMAINING]->setText(QString::number(aItem.getAmountRemaining()));
        }

        updateQuantity();
        updateCost();
    }

    ~SupplyRow()
    {
        mAllItems.clear();
    }


    Item getItem() const
    {
        bool priceOk = true;
        bool broughtOk = true;
        bool remainingOK = true;

        const QString& name = mAllItems[Columns::ITEM]->text();

        const currency::String& priceText = mAllItems[Columns::PRICE]->text();

        int price = priceText.toInt(&priceOk);

        int brought = mAllItems[Columns::BROUGHT]->text().toInt(&broughtOk);
        int remaining = mAllItems[Columns::REMAINING]->text().toInt(&remainingOK);

        price = (priceOk ? price : -1);
        brought = (broughtOk ? brought : -1);
        remaining = (remainingOK ? remaining : -1);

        Item out = Item(name, price, brought, remaining);

        return out;
    }

    int getCost() const
    {
        int cost = -1;

        bool priceOk = true;
        const currency::String& priceText = mAllItems[Columns::PRICE]->text();

        int price = priceText.toInt(&priceOk);

        if(priceOk)
        {
            cost = (price * getQuantity());
        }

        return cost;
    }

    void addToTable(QTableWidget* table, int row)
    {
        for(int col = Columns::BEGIN; col <= Columns::END; ++col)
        {
            table->setItem(row, col, mAllItems[col]);
        }
    }

    void updateCost()
    {
        int cost = getCost();

        QString costString = currency::String::toString(cost);
        mAllItems[Columns::COST]->setText(costString);
    }

    void updateQuantity()
    {
        int quantity = getQuantity();
        mAllItems[Columns::QUANTITY]->setText(QString::number(quantity));

        updateCost();
    }

private:
    void init()
    {
        for(int i = Columns::BEGIN; i <= Columns::END; ++i)
        {
            mAllItems.append(new QTableWidgetItem());
            mAllItems[i]->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        }

        QTableWidgetItem* qty = mAllItems[Columns::QUANTITY];
        QTableWidgetItem* cost = mAllItems[Columns::COST];

        qty->setFlags(qty->flags() & ~Qt::ItemIsEnabled);
        cost->setFlags(cost->flags() & ~Qt::ItemIsEnabled);
    }

    int getQuantity() const
    {
        int qty = -1;
        bool broughtOk = true;
        bool remainingOK = true;

        int brought = mAllItems[Columns::BROUGHT]->text().toInt(&broughtOk);
        int remaining = mAllItems[Columns::REMAINING]->text().toInt(&remainingOK);

        if( broughtOk && remainingOK)
        {
            qty = (brought - remaining);
        }

        return qty;
    }
private:
    QList<QTableWidgetItem*> mAllItems;
};

SupplyTableWidget::SupplyTableWidget(QWidget* parent) :
    QWidget (parent),
    mpForm(new Ui::SupplyTableWidget)
{
    mpForm->setupUi(this);

    mpForm->supplyTable->setColumnWidth(Columns::ITEM, 150);
    mpForm->supplyTable->setColumnWidth(Columns::PRICE, 80);
    mpForm->supplyTable->setColumnWidth(Columns::BROUGHT, 80);
    mpForm->supplyTable->setColumnWidth(Columns::REMAINING, 80);
    mpForm->supplyTable->setColumnWidth(Columns::QUANTITY, 80);
    mpForm->supplyTable->setColumnWidth(Columns::COST, 80);

    QCompleter* completer = new QCompleter(SupplyManager::instance()->getAllKnownSupplies());
    completer->setCompletionMode(QCompleter::InlineCompletion);

    mpForm->supplyTable->setItemDelegateForColumn(Columns::ITEM, new CompleterDelegate(completer, this));
    mpForm->supplyTable->setItemDelegateForColumn(Columns::PRICE, new CurrencyDelegate(this));
    mpForm->supplyTable->setItemDelegateForColumn(Columns::BROUGHT, new IntegerDelegate(this));
    mpForm->supplyTable->setItemDelegateForColumn(Columns::REMAINING, new IntegerDelegate(this));
    mpForm->supplyTable->setItemDelegateForColumn(Columns::QUANTITY, new UneditableDelegate(this));
    mpForm->supplyTable->setItemDelegateForColumn(Columns::COST, new UneditableDelegate(this));

    connect(mpForm->btnAddSupplies, &QPushButton::clicked, this, &SupplyTableWidget::addSupplyClicked);
    connect(mpForm->btnRemoveSupplies, &QPushButton::clicked, this, &SupplyTableWidget::removeSupplyClicked);

    connect(mpForm->supplyTable, &QTableWidget::cellChanged, this, &SupplyTableWidget::cellUpdated);
}

SupplyTableWidget::~SupplyTableWidget()
{
    qDeleteAll(mSupplyWidgets);
    mSupplyWidgets.clear();

    delete mpForm;
    mpForm = nullptr;
}

Supplies SupplyTableWidget::getAllSupplies() const
{
    Supplies out = Supplies();

    foreach(const SupplyRow* supply, mSupplyWidgets)
    {
        out.addItem(supply->getItem());
    }

    return out;
}

void SupplyTableWidget::setSupplies(const Supplies& supplies, bool historical)
{
    const SuppliesList& suppliesList = supplies.getSuppliesList();

    int numberSupplies = suppliesList.size();
    mpForm->supplyTable->setRowCount(numberSupplies);

    for(int r = 0; r < numberSupplies; ++r)
    {
        SupplyRow* newRow = new SupplyRow(suppliesList[r], historical);
        newRow->addToTable(mpForm->supplyTable, r);

        mSupplyWidgets.append(newRow);
    }


    updateTotalCost();
}

void SupplyTableWidget::keyPressEvent(QKeyEvent *event)
{
    if ((event->key() == Qt::Key_Enter) ||
        (event->key() == Qt::Key_Return))
    {
        QList<QTableWidgetItem*> selectedItems = mpForm->supplyTable->selectedItems();
        QTableWidgetItem* i = selectedItems.at(0);

        int row = i->row();
        int col = i->column();

        col = (col < Columns::REMAINING ? col + 1 : Columns::BEGIN); //Cells to right of remaining can't be changed.

        if(col == Columns::BEGIN)
        {
            row = (row < (mpForm->supplyTable->rowCount() - 1)? row + 1 : 0);
        }

        QTableWidgetItem* it = mpForm->supplyTable->item(row, col);
        mpForm->supplyTable->setCurrentItem(it);
    }
    else
    {
        QWidget::keyPressEvent(event);
    }
}

void SupplyTableWidget::addSupplyClicked()
{
    int rowNumber = mSupplyWidgets.size();

    mpForm->supplyTable->setRowCount(rowNumber + 1);

    SupplyRow* row = new SupplyRow();
    row->addToTable(mpForm->supplyTable, rowNumber);

    mSupplyWidgets.append(row);
    updateTotalCost();
}

void SupplyTableWidget::removeSupplyClicked()
{
    QList<QTableWidgetItem*> selectedItems = mpForm->supplyTable->selectedItems();
    foreach(QTableWidgetItem* item, selectedItems)
    {
        int row = item->row();

        mpForm->supplyTable->removeRow(row);
        delete mSupplyWidgets.takeAt(row);
    }

    updateTotalCost();
}

void SupplyTableWidget::cellUpdated(int row, int column)
{
    bool validRow = (row >= 0 && row < mSupplyWidgets.size());
    bool validCol = ( (column == Columns::PRICE) ||
                      (column == Columns::REMAINING) ||
                      (column == Columns::BROUGHT));

    if(validRow && validCol)
    {
        SupplyRow* supplyRow = mSupplyWidgets[row];

        if(column == Columns::PRICE)
        {
            supplyRow->updateCost();
        }
        else if((column == Columns::BROUGHT) ||
                (column == Columns::REMAINING))
        {
            supplyRow->updateQuantity();
        }

        updateTotalCost();
    }
}

void SupplyTableWidget::updateTotalCost()
{
    int totalCost = 0;

    foreach(const SupplyRow* r, mSupplyWidgets)
    {
        totalCost += r->getCost();
    }

    emit totalCostUpdated(totalCost);
}
