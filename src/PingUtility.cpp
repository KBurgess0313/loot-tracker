#include "PingUtility.h"

#include <iostream>
#include <winsock.h>
#include <windowsx.h>
#include <string>

typedef struct {
    unsigned char Ttl;                         // Time To Live
    unsigned char Tos;                         // Type Of Service
    unsigned char Flags;                       // IP header flags
    unsigned char OptionsSize;                 // Size in bytes of options data
    unsigned char *OptionsData;                // Pointer to options data
} IP_OPTION_INFORMATION, * PIP_OPTION_INFORMATION;

typedef struct {
    DWORD Address;                             // Replying address
    unsigned long  Status;                     // Reply status
    unsigned long  RoundTripTime;              // RTT in milliseconds
    unsigned short DataSize;                   // Echo data size
    unsigned short Reserved;                   // Reserved for system use
    void *Data;                                // Pointer to the echo data
    IP_OPTION_INFORMATION Options;             // Reply options
} IP_ECHO_REPLY, * PIP_ECHO_REPLY;

struct WSASetup
{
  WSASetup()
  {
    WSAStartup(MAKEWORD(1, 1), &wsaData);
  }
  ~WSASetup()
  {
    WSACleanup();
  }

  WSAData wsaData;
};

int pingUrl(const QString& url)
{
  static WSASetup setup;

  static std::string s = "ICMP.DLL";
  static std::wstring stemp = std::wstring(s.begin(), s.end());
  LPCWSTR dll = stemp.c_str();

  // Load the ICMP.DLL
  HINSTANCE hIcmp = LoadLibrary(dll);
  if (hIcmp == nullptr) {
      std::cerr << "Unable to locate ICMP.DLL!" << std::endl;
      return -1;
  }

  // Look up an IP address for the given host name
  struct hostent* phe;
  if ((phe = gethostbyname(url.toStdString().c_str())) == nullptr) {
      std::cerr << "Could not find IP address for " << url.toStdString().c_str() << std::endl;
      return -1;
  }

  // Get handles to the functions inside ICMP.DLL that we'll need
  typedef HANDLE (WINAPI* pfnHV)(VOID);
  typedef BOOL (WINAPI* pfnBH)(HANDLE);
  typedef DWORD (WINAPI* pfnDHDPWPipPDD)(HANDLE, DWORD, LPVOID, WORD,
          PIP_OPTION_INFORMATION, LPVOID, DWORD, DWORD); // evil, no?
  pfnHV pIcmpCreateFile;
  pfnBH pIcmpCloseHandle;
  pfnDHDPWPipPDD pIcmpSendEcho;
  pIcmpCreateFile = (pfnHV)GetProcAddress(hIcmp,
          "IcmpCreateFile");
  pIcmpCloseHandle = (pfnBH)GetProcAddress(hIcmp,
          "IcmpCloseHandle");
  pIcmpSendEcho = (pfnDHDPWPipPDD)GetProcAddress(hIcmp,
          "IcmpSendEcho");
  if ((pIcmpCreateFile == 0) || (pIcmpCloseHandle == 0) ||
          (pIcmpSendEcho == 0)) {
      std::cerr << "Failed to get proc addr for function." << std::endl;
      return -1;
  }

  // Open the ping service
  HANDLE hIP = pIcmpCreateFile();
  if (hIP == INVALID_HANDLE_VALUE) {
      std::cerr << "Unable to open ping service." << std::endl;
      return -1;
  }

  // Build ping packet
  char acPingBuffer[64];
  memset(acPingBuffer, '\xAA', sizeof(acPingBuffer));
  PIP_ECHO_REPLY pIpe = (PIP_ECHO_REPLY)GlobalAlloc(
          GMEM_FIXED | GMEM_ZEROINIT,
          sizeof(IP_ECHO_REPLY) + sizeof(acPingBuffer));
  if (pIpe == 0) {
      std::cerr << "Failed to allocate global ping packet buffer." << std::endl;
      return -1;
  }
  pIpe->Data = acPingBuffer;
  pIpe->DataSize = sizeof(acPingBuffer);

  // Send the ping packet
  DWORD dwStatus = pIcmpSendEcho(hIP, *((DWORD*)phe->h_addr_list[0]),
          acPingBuffer, sizeof(acPingBuffer), NULL, pIpe,
          sizeof(IP_ECHO_REPLY) + sizeof(acPingBuffer), 5000);

  int rtt = -1;

  if (dwStatus != 0)
  {
      rtt = int(pIpe->RoundTripTime);
  }
  else {
      std::cerr << "Error obtaining info from ping packet." << std::endl;
  }

  // Shut down...
  GlobalFree(pIpe);
  FreeLibrary(hIcmp);

  return rtt;
}
