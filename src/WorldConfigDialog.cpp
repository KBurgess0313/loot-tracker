#include "WorldConfigDialog.h"

#include <QInputDialog>

#include "SaveFile.h"
#include "World.h"
#include "WorldManager.h"

#include "ui_WorldConfigDialog.h"

namespace
{
  enum Columns
  {
    WORLD_COL = 0,
    PING_COL,
    SIZE_COL
  };
}

WorldConfigDialog::WorldConfigDialog(QWidget* aParent) :
  QDialog(aParent),
  mpForm(new Ui::WorldConfigDialog)
{
  mpForm->setupUi(this);

  connect(mpForm->btnAdd, &QPushButton::clicked, this, &WorldConfigDialog::addWorldClicked);
  connect(mpForm->btnDelete, &QPushButton::clicked, this, &WorldConfigDialog::removeWorldClicked);

  connect(WorldManager::instance(), &WorldManager::pingUpdate, this, &WorldConfigDialog::worldPingUpdate);

  init();
}

WorldConfigDialog::~WorldConfigDialog()
{
  delete mpForm;
  mpForm = nullptr;
}

void WorldConfigDialog::init()
{
  const QList<World*>& worlds = WorldManager::instance()->getSupportedWorlds();

  foreach(const World* w, worlds)
  {
    addWorld(*w);
  }
}

void WorldConfigDialog::addWorldClicked()
{
  int worldNumber = QInputDialog::getInt(nullptr, "Add World",
                                       "Enter World Number:");

  World* world = new World(worldNumber);

  WorldManager::instance()->addWorld(world);

  addWorld(*world);
}

void WorldConfigDialog::addWorld(const World& world)
{
  mpForm->worldTable->setSortingEnabled(false);

  int rowNumber = mpForm->worldTable->rowCount();
  mpForm->worldTable->setRowCount(rowNumber + 1);

  QTableWidgetItem* worldItem = new QTableWidgetItem();
  QTableWidgetItem* pingItem = new QTableWidgetItem();
  QTableWidgetItem* sizeItem = new QTableWidgetItem();

  int worldNumber = world.getWorldNumber();
  int averagePing = world.getAveragePing();
  QString pingText = ((averagePing != -1) ? QString::number(averagePing) : "...");

  worldItem->setData(Qt::UserRole, worldNumber);
  worldItem->setText(QString("%1").arg(worldNumber));
  pingItem->setText(pingText);
  sizeItem->setText("0");

  mpForm->worldTable->setItem(rowNumber, WORLD_COL, worldItem);
  mpForm->worldTable->setItem(rowNumber, PING_COL, pingItem);
  mpForm->worldTable->setItem(rowNumber, SIZE_COL, sizeItem);

  mpForm->worldTable->setSortingEnabled(true);
}

void WorldConfigDialog::removeWorldClicked()
{
  WorldManager* wMgr = WorldManager::instance();

  QList<QTableWidgetItem*> selectedItems = mpForm->worldTable->selectedItems();
  foreach(QTableWidgetItem* item, selectedItems)
  {
    int row = item->row();

    int worldNumber = mpForm->worldTable->item(row, WORLD_COL)->data(Qt::UserRole).toInt();

    wMgr->removeWorld(worldNumber);

    mpForm->worldTable->removeRow(row);
  }
}

void WorldConfigDialog::worldPingUpdate(const World& w)
{
  int wNumber = w.getWorldNumber();
  int wPing = w.getAveragePing();

  int rowNumber = mpForm->worldTable->rowCount();

  bool found = false;

  for(int i = 0; ((i < rowNumber) && !found); ++i)
  {
    QTableWidgetItem* worldItem = mpForm->worldTable->item(i, WORLD_COL);

    if(worldItem->data(Qt::UserRole).toInt() == wNumber)
    {
      QTableWidgetItem* pingItem = mpForm->worldTable->item(i, PING_COL);
      QTableWidgetItem* sizeItem = mpForm->worldTable->item(i, SIZE_COL);

      const QString& pingText = ((wPing != -1) ? QString::number(wPing) : "...");
      const QString& size = QString::number(w.getSampleSize());

      pingItem->setText(pingText);
      sizeItem->setText(size);

      found = true;
    }
  }
}
