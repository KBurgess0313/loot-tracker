#include "Supplies.h"

#include <QSettings>

namespace
{
    const QString SETTINGS_HEADER = QString("supplies");

    const QString NAME = QString("name");
    const QString PRICE = QString("price");
    const QString BROUGHT = QString("brought");
    const QString REMAINING = QString("remaining");

    //As of 1.4, this is an outdated variable that is only used
    //for older builds.
    const QString QUANTITY = QString("quantity");
}

Item::Item(QSettings& aSettings)
{
  mName = aSettings.value(NAME, "Error").toString();
  mPrice = aSettings.value(PRICE, -1).toInt();

  //This logical branch is to handle old logic.  Previously, we didn't
  //save off brought/remaining from one run to the next.  Just in case
  //someone uses an old build, this will handle that change the first time.
  if(aSettings.contains(QUANTITY))
  {
    mBrought = aSettings.value(QUANTITY, -1).toInt();
    mRemaining = 0;
  }
  else
  {
    mBrought = aSettings.value(BROUGHT, -1).toInt();
    mRemaining = aSettings.value(REMAINING, -1).toInt();
  }
}

Item::Item(const QString& name, int price, int brought, int remaining) :
    mName(name),
    mPrice(price),
    mBrought(brought),
    mRemaining(remaining)
{
}

void Item::save(QSettings& aItem) const
{
    aItem.setValue(NAME, mName);
    aItem.setValue(PRICE, mPrice);
    aItem.setValue(BROUGHT, mBrought);
    aItem.setValue(REMAINING, mRemaining);
}

const QString& Item::getName() const
{
  return mName;
}

void Item::setPrice(const int aPrice)
{
  mPrice = aPrice;
}

int Item::getPrice() const
{
  return mPrice;
}

void Item::setAmountBrought(const int brought)
{
  mBrought = brought;
}

const int Item::getAmountBrought() const
{
  return mBrought;
}

void Item::setAmountRemaining(const int remaining)
{
  mRemaining = remaining;
}

const int Item::getAmountRemaining() const
{
  return mRemaining;
}

int Item::getAmountUsed() const
{
    return (mBrought - mRemaining);
}

int Item::getCost() const
{
    return ((mBrought - mRemaining) * mPrice);
}

Supplies::Supplies(const SuppliesList& aSupplies) :
    mSupplies(aSupplies)
{
}

Supplies::Supplies(QSettings& aSettings)
{
    int numSupplies = aSettings.beginReadArray(SETTINGS_HEADER);

    for(int i = 0; i < numSupplies; ++i)
    {
        aSettings.setArrayIndex(i);
        Item item = Item(aSettings);

        mSupplies.append(item);
    }

    aSettings.endArray();
}

void Supplies::save(QSettings& aSettings) const
{
    aSettings.beginWriteArray(SETTINGS_HEADER);

    int numSupplies = mSupplies.size();
    for(int i = 0; i < numSupplies; ++i)
    {
        aSettings.setArrayIndex(i);

        mSupplies[i].save(aSettings);
    }

    aSettings.endArray();
}

void Supplies::addItem(const Item& aItem)
{
    mSupplies.append(aItem);
}

void Supplies::removeItem(const QString& name)
{
    SuppliesList::iterator it = mSupplies.begin();
    SuppliesList::iterator end = mSupplies.end();

    for( ; it != end; ++it)
    {
        if(it->getName() == name)
        {
            it = mSupplies.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

int Supplies::getTripCost() const
{
    int tripCost = 0;

    SuppliesList::const_iterator it = mSupplies.begin();
    SuppliesList::const_iterator end = mSupplies.end();

    for( ; it != end; ++it)
    {
        tripCost += it->getCost();
    }

    return tripCost;
}

const SuppliesList& Supplies::getSuppliesList() const
{
    return mSupplies;
}
